package com.quinto.Exceptions;

import java.io.FileNotFoundException;
import java.io.IOException;

// IMPORTANT
// 1. Neither Errors nor RuntimeExceptions are used for recoverable situations.
// 2. RuntimeExceptions should be identified during testing and eliminated by fixing the code,
//    while Errors should be eliminated by fixing the environment.
// 3. The exception parameter in a multi-catch clause is implicitly final.

public class HandlingExceptions {
    /*// try-catch
    void explore(){
        try{
            fall();
            System.out.println("never get here");
        }catch (RuntimeException e){
            //getUp(); assume that exists
        }
        //seeAnimals(); assume that exists
    }

    void fall(){
        throw new RuntimeException();
    }

}

class AnimalsOutForWalk extends RuntimeException{

}
class ExhibitClosed extends RuntimeException{

}
class ExhibitClosedForLunch extends ExhibitClosed{

}
class EncadenandoCatchs{

    // TENER EN CUENTA EL ORDEN DE LOS CATCH


    public void visitPorcupine(){
        try {
            // seeAnimal(); assume that exists
        }catch (AnimalsOutForWalk e){
            System.out.println("try back later");
        }catch (ExhibitClosed e){
            System.out.println("not today");
        }
    }

    // Deben ir de menor a mayor, osea de mas especificas a mas genericas
    public void correctOrder(){
        try {
            // seeAnimal(); assume that exists
        }catch (ExhibitClosedForLunch e){ // subclass
            System.out.println("try back later");
        }catch (ExhibitClosed e){ // superclass
            System.out.println("not today");
        }
    }

    public void incorrectOrder(){
        try {
            // seeAnimal(); assume that exists
        }catch (ExhibitClosed e){ // subclass
            System.out.println("not today");
        }catch (ExhibitClosedForLunch e){ // NO COMPILE
            System.out.println("try back later");
        }
    }

    public void incorrectOrder2(){
        try{
            System.out.println("Eo");
        }catch (IllegalArgumentException e){

        }catch (NumberFormatException e){ // NO COMPILE

        }
    }

    // SCOPE VARIABLES
    public void visit(){
        try {

        }catch (NumberFormatException el){
            System.out.println(el);
        }catch (IllegalArgumentException en){
            System.out.println(el); // NO COMPILE
        }
    }

    // APLICANDO MULTICATCH
    public static void main(String[] args) {
        try {
            System.out.println(Integer.parseInt(args[1]));
        }catch (ArrayIndexOutOfBoundsException | NumberFormatException e){ // only ine variable name, no debe haber relacion entre las excepciones
            System.out.println("Missing or invalid input");
        }

        try {
            throw new IOException();
        }catch (FileNotFoundException | IOException p){ // Son excepciones relacionadas

        }
    }

    // finally statement - se ejecuta siempre haya o no exception
    void explore(){
        try {
            seeAnimals();
            fall();
        }catch (Exception e){
            getHugFromDaddy();
        }finally {
            seeMoreAnimals();
        }
        goHome();
    }

    // no puede haber un try sin finally o catch
    void badTry(){
        try {
            System.out.println("Hello");
        }
    }

    void example(){
        StringBuilder builder = new StringBuilder();
        try {
            builder.append("t");
        }catch (Exception e){
            builder.append("c");
        }finally {
            builder.append("f");
        }
        builder.append("a");
        System.out.println(builder.toString()); // print "tfa"
    }

    int example2(){
        try {
            // Optional throw an exception here
            System.out.println("1");
            return -1;
        }catch (Exception e){
            System.out.println("2");
            return -2;
        }finally {
            System.out.println("3");
            return -3; // interrumple la sentencia del try o del catch
        }
        // Caso sin exception: imprime "1" y "3" y luego retorna -3 que retorna el finally
        // Caso con exception: imprime "2" y "3" y luego retorna -3 que retorna el finally
    }

    // el System.exit(0) hace que no se ejecute el finally
    void noFinally(){
        try {
            System.exit(0);
        }finally {
            System.out.println("Never going to get here");
        }
    }*/
}

// Sobreescribir metodos que arrojan exception
class CanNotHopException extends Exception{

}

class Hopper{
    public void hop(){

    }

    public void hop2() throws Exception{

    }

    public void hop3(){

    }
}

class Bunny extends Hopper{
    // El hijo no puedes lanzar una excelcion checkeada que no tenga el padre.
    /*public void hop() throws CanNotHopException{ // I CAN´T DO IT

    }*/

    // Un metodo que se sobreescribe solo puede declarar 0 o varias de las
    //  excepciones que tenga el padre.
    // La excepcion puede ser igual o hija, eso en cuanto a las checkeadas
    public void hop2() throws NumberFormatException{

    }

    // Si la excepcion es unchecked el hijo si puede lanzarla asi no la tenga el padre
    public void hop3() throws RuntimeException{

    }
}

