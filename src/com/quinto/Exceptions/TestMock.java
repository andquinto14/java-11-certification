package com.quinto.Exceptions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;

/*class TestClass
{
    public static void main(String[] args) throws IOException {
        final Reader reader = new FileReader("aaa.a");  //1
        try(reader){
            reader.read(); //2
        }finally{
            reader.read(); //3
        }
        reader.read(); //4
    }
}*/
/*
class M{
    public static void main(String[] args) throws IOException {
        FileInputStream tempFis = null;
        try(FileInputStream fis = new FileInputStream("c:\\temp\\test.text")){
            System.out.println(fis);
            tempFis = fis;
        } catch (IOException e) {
            e.printStackTrace();
        }finally{  tempFis.close(); }

        //line 1
    }
}*/
/*
class Test{
    public int luckyNumber(int seed){
        if(seed > 10) return seed%10;
        int x = 0;
        try{
            if(seed%2 == 0) throw new Exception("No Even no.");
            else return x;
        }
        catch(Exception e){
            return 3;
        }
        finally{
            return 7;
        }
    }

    public static void main(String args[]){
        int amount = 100, seed = 6;
        switch( new Test().luckyNumber(6) ){
            case 3: amount = amount * 2;
            case 7: amount = amount * 2;
            case 6: amount = amount + amount;
            default :
        }
        System.out.println(amount);
    }
}*/
/*
class FileCopier {
    public static void copy(String records1, String records2) throws IOException {
        try (
                InputStream is = new FileInputStream(records1);
                OutputStream os = new FileOutputStream(records2);) {
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
                System.out.println("Read and written bytes " + bytesRead);
            }
        } catch (IOException | IndexOutOfBoundsException e) {
            e = new FileNotFoundException();
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        copy("c:\\temp\\test1.txt", "c:\\temp\\test2.txt");

    }
}*/
class Device implements AutoCloseable {
    boolean open = false;
    int index;

    public Device(int index) {
        this.index = index;
        open = true;
    }

    public void write() throws IOException {
        throw new RuntimeException("Can't write!");
    }

    public void close() {
        open = false;
        System.out.println("Device closed " + index);
    }

    public static void main(String[] args) {
        Device d1 = new Device(1);
        try (d1;
             Device d2 = new Device(2);
             Device d3 = new Device(3)) {
            d2.write();
            d1.close();
        } catch (Exception e) {
            System.out.println("Got Exception " + e.getMessage());
        }
    }

    //OUTPUT:
    // Device closed 3
    // Device closed 2
    // Device closed 1
    // Got Exception Can't write!
}

class TestClass {

    public static void doStuff() throws Exception {
        System.out.println("Doing stuff...");
        double number = Math.random();
        System.out.println(number);
        if (number > 0.4) {
            throw new Exception("Too high!");
        }
        System.out.println("Done stuff.");
    }

    public static void main(String[] args) throws Exception {
        doStuff();
        System.out.println("Over");
    }

    //OUTPUT 1, number < 0.4:
    // Doing stuff...
    // 0.3909271087032835
    // Done stuff.
    // Over

    //OUTPUT 2, number > 0.4:
    // Doing stuff...
    //  0.689993349563844
    // Exception in thread "main" java.lang.Exception: Too high!
}

class Test {
    public int luckyNumber(int seed) {
        if (seed > 10) return seed % 10;
        int x = 0;
        try {
            if (seed % 2 == 0) throw new Exception("No Even no.");
            else return x;
        } catch (Exception e) {
            return 3;
        } finally {
            return 7;
        }
    }

    public static void main(String args[]) {
        int amount = 100, seed = 6;
        switch (new Test().luckyNumber(6)) {
            case 3:
                amount = amount * 2;
            case 7:
                amount = amount * 2;
            case 6:
                amount = amount + amount;
            default:
        }
        System.out.println(amount); // OUTPUT: 400
    }
}

class TestClass2 {
    public static void m1() throws Exception {
        throw new Exception("Exception from m1");
    }

    public static void m2() throws Exception {
        try {
            m1();
        } catch (Exception e) {
            //Can't do much about this exception so rethrow it
            throw e;
        } finally {
            throw new RuntimeException("Exception from finally");
        }

    }

    public static void main(String[] args) {
        try {
            m2();
        } catch (Exception e) {
            Throwable[] ta = e.getSuppressed();
            for (Throwable t : ta) {
                System.out.println(t.getMessage());
            }
        }
    }

    // OUTPUT:
    // Itl will not print anything because the only exception thrown is from the finally block
    // on method m2, then, there is not suppressed exceptions to iterate in the for loop
}

class Scrap {
    public static void main(String[] args) {
        try {
            if (args.length == 0) m2();
            else m3();
        } catch (IOException | IndexOutOfBoundsException e) {

        }

    }

    public static void m2() throws IOException {
        throw new FileNotFoundException();
    }

    public static void m3() throws IndexOutOfBoundsException {
        throw new IndexOutOfBoundsException();
    }
}

class TestClass3 {
    public static void main(String[] args) throws Exception {
        TestClass3 tc = new TestClass3();
        tc.myMethod();
    }

    public void myMethod() throws Exception {
        yourMethod();
    }

    public void yourMethod() throws Exception {
        throw new Exception();
    }
}

class Test4 {
    public static void main(String[] args) {
        int j = 1;
        try {
            int i = doIt() / (j = 2);
        } catch (Exception e) {
            System.out.println(" j = " + j);
        }
    }

    public static int doIt() throws Exception {
        throw new Exception("FORGET IT");
    }
}

class MyException extends Exception {
}

class TestClass4 {
    public void myMethod() throws Exception {
        throw new MyException();
    }
}

class FinallyTest {
    public static void main(String args[]) throws Exception {
        try {
            m1();
            System.out.println("A");
        } finally {
            System.out.println("B");
        }
        System.out.println("C");
    }

    public static void m1() throws Exception {
        throw new Exception();
    }

    public void processArray(int[] values) {
        int sum = 0;
        int i = 0;
        try {
            while (values[i] < 100) {
                sum = sum + values[i];
                i++;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
        }
        System.out.println("sum = " + sum);
    }

}

class Testd {
    static int i1, i2, i3;

    public static void main(String[] args) {
        try {
            test(i1 = 1, oops(i2 = 2), i3 = 3);
        } catch (Exception e) {
            System.out.println(i1 + " " + i2 + " " + i3);
        }
    }

    static int oops(int i) throws Exception {
        throw new Exception("oops");
    }

    static int test(int a, int b, int c) {
        return a + b + c;
    }

    public static void copy(String records1, String records2) {
        try (
                InputStream is = new FileInputStream(records1);
                OutputStream os = new FileOutputStream(records2);) {

            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
                System.out.println("Read and written bytes " + bytesRead);
            }
        } catch (IOException | RuntimeException e) {
            e.printStackTrace();
        }
    }
}

class Testr {
    public static void main(String[] args) {
        int[][] a = {{00, 01}, {10, 11}};
        int i = 99;
        try {
            a[val()][i = 1]++;
        } catch (Exception e) {
            System.out.println(i + ", " + a[1][1]);
        }
    }

    static int val() throws Exception {
        throw new Exception("unimplemented");
    }
}

/*class FileCopier {
    public static void copy(String records1, String records2)  {
        try {
            InputStream is = new FileInputStream(records1);
            OutputStream os = new FileOutputStream(records2);
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
                System.out.println("Read and written bytes " + bytesRead);
            }
        } catch (FileNotFoundException | IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        copy("c:\\temp\\test1.txt", "c:\\temp\\test2.txt");
    }
}*/

class Testt {
    static String j = "";

    public static void method(int i) {
        try {
            if (i == 2) {
                throw new Exception();
            }
            j += "1";
        } catch (Exception e) {
            j += "2";
            return;
        } finally {
            j += "3";
        }
        j += "4";
    }

    public static float parseFloat(String s) {
        float f = 0.0f;
        try {
            f = Float.valueOf(s).floatValue();
            return f;
        } catch (NumberFormatException nfe) {
            f = Float.NaN;
            return f;
        } finally {
            f = 10.0f;
            return f;
        }
    }

    public static void main(String args[]) {
        method(1);
        method(2);
        System.out.println(j);
        System.out.println(parseFloat("0.0"));
    }
}

class Testtt {
    public static void main(String[] args) {
        int j = 1;
        try {
            int i = doIt() / (j = 2);
        } catch (Exception e) {
            System.out.println(" j = " + j);
        }
    }

    public static int doIt() throws Exception {
        throw new Exception("FORGET IT");
    }
}

class MyExceptionn extends Throwable {
}

class MyException1 extends MyExceptionn {
}

class MyException2 extends MyExceptionn {
}

class MyException3 extends MyException2 {
}

class ExceptionTest {
    void myMethod() throws MyExceptionn {
        throw new MyException3();
    }

    public static void main(String[] args) {
        ExceptionTest et = new ExceptionTest();
        try {
            et.myMethod();
        } catch (MyExceptionn me) {
            System.out.println("MyException thrown");
        //} catch (MyException3 me3) {
            System.out.println("MyException3 thrown");
        } finally {
            System.out.println(" Done");
        }
    }

    public void method1() throws Exception {
        try {

        } catch (Exception throwable) {

        }
    }
}

class FinallyTestw{
    public static void main(String args[]) throws Exception{
        try{
            m1();
            System.out.println("A");
        }
        finally{
            System.out.println("B");
        }
        System.out.println("C");
    }
    public static void m1() throws Exception { throw new Exception(); }
}

class TestClasss{
    public static void main(String args[]) throws Exception {
        Exception e = null;
        throw e;
    }
}

class TestClasTs{
    public static void main(String[] args) throws IOException {

        final InputStream fis = new FileInputStream("c:\\temp\\test.txt");
        long l = 0;
        try(fis){
            l = fis.read();//1
        }finally{
            l = fis.read();//2
        }
        l = fis.read();//3
        System.out.println(l);//4
    }
}

class TestClashs{
    public static void main(String args[]){
        int k = 0;
        try{
            int i = 5/k;
        }
        catch (ArithmeticException e){
            System.out.println("1");
        }
        catch (RuntimeException e){
            System.out.println("2");
            return ;
        }
        catch (Exception e){
            System.out.println("3");
        }
        finally{
            System.out.println("4");
        }
        System.out.println("5");
    }
}
