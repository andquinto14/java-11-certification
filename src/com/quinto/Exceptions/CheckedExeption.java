package com.quinto.Exceptions;

import java.io.IOException;

// RULES CHECKED EXEPTIONS
// 1. Patearlas (metodo 1) o manejarlas (metodo 2)
// 2. Son hijas de Exception pero no de Runtime

class CheckedExeption {
    void method1(int distance) throws IOException {
        if (distance > 10) {
            throw new IOException();
        }
    }

    void method2(int distance) throws IOException {
        try {
            if (distance > 10) {
                throw new IOException();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class throwing{
    void test() throws Exception {
        Exception e = new RuntimeException();
        throw e;
        // throw RuntimeException(); NO COMPILE
    }

    void test2(){
        try{
            throw new RuntimeException();
            // throw new IndexOutOfBoundsException(); NO COMPILE, UNREACHABLE CODE
        }catch (Exception e){

        }
    }
}
