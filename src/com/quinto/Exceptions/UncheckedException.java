package com.quinto.Exceptions;

// REGLAS NO CHECKED
// No necesitan try catch ni manejar la exception
// Suceden en tiempo de ejecucion por algo inesperado
// Son hijas de Runtime

class UncheckedException {
    void fall(String input){
        System.out.println(input.toLowerCase());
    }
}
