package com.quinto.Exceptions;

public class Errors {
    // ExceptionInInitializerError
    static {
        int[] counts = new int[3];
        int num = counts[-1]; // java.lang.ExceptionInInitializerError. Caused by: java.lang.ArrayIndexOutOfBoundsException: Index -1 out of bounds for length 3
    }

    // StackOverflowError
    public static void doNotThis(int num){
        doNotThis(1); // java.lang.StackOverflowError
    }

    // NoClassDefFoundError

    public static void main(String[] args) {

    }
}
