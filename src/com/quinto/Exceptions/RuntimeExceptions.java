package com.quinto.Exceptions;

// Son unchecked
//

public class RuntimeExceptions {
    public static void main(String[] args) {
        // ArithmeticException
        int answer = 11 / 0;
        System.out.println(answer); // java.lang.ArithmeticException: / by zero

        // ArrayIndexOutBoundsException
        int[] counts = new int[3];
        System.out.println(counts[-1]); //java.lang.ArrayIndexOutOfBoundsException: Index -1 out of bounds for length 3

        // ClassCastException
        String type = "moose";
        // Integer number = (Integer) type; NO COMPILE
        String type2 = type;
        Object obj = type2;
        Integer number = (Integer) obj; // java.lang.ClassCastException: class java.lang.String cannot be cast to class java.lang.Integer

        // NullPointerException
        String name = null;
        System.out.println(name.length()); // java.lang.NullPointerException

        // IllegalArgumentException
        class m{
            void setNumberEggs(int numberEggs){
                if(numberEggs < 0)
                    throw new IllegalArgumentException("# eggs must not be negative");
            }
        }

        // NumberFormatException - Hija de IllegalArgumentException
        Integer.parseInt("abc"); // java.lang.NumberFormatException: For input string: "abc"
    }
}
