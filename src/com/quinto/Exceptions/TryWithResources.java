package com.quinto.Exceptions;

import java.io.Closeable;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class TryWithResources {
    public static void main(String[] args) {
        TryWithResources tryWithResources = new TryWithResources();
        try {
            tryWithResources.readFile("myile.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readFile(String file) throws IOException {
        //Objects inside try with resource statement have to implement the closeable interface
        try (FileInputStream is = new FileInputStream(file)) {

        }
    }

    // Alcance try-with resource
    public void badTry(String file) throws IOException {
        try (Scanner in = new Scanner(file)) {
            in.nextLine();
        } finally {
            // in.nextLine(); in is not reachable
        }
    }
}

class MyFileClass implements AutoCloseable {
    // Orden de operacion
    // 1. Los resursos se cierran despues de que la sentencia catch finaliza
    // 2. Los recursos se cierran en el orden inverso en el cual se declaran.
    private final int num;

    public MyFileClass(int num) {
        this.num = num;
    }

    @Override
    public void close() throws Exception {
        System.out.println("Closing: " + num);
    }

    public static void main(String[] args) {
        try (var a1 = new MyFileClass(1); MyFileClass a2 = new MyFileClass(2)) {
            throw new RuntimeException();
        } catch (Exception e) {
            System.out.println("Ex");
        } finally {
            System.out.println("finally");
        }
    }
}