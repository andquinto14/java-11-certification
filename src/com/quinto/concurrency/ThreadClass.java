package com.quinto.concurrency;

public class ThreadClass implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            System.out.println("Printing record: " + i);
        }
    }

    public static void main(String[] args) {
        new Thread(new ThreadClass()).start(); // La tearea se ejecuta en un nuevo thread
    }
}

class ReadInventory extends Thread {
    @Override
    public void run() {
        System.out.println("Printing inventory");
    }

    public static void main(String[] args) {
        (new ReadInventory()).start(); // La tearea se ejecuta en un nuevo thread
    }
}

// Using start method -> Se crea un nuevo hilo, la ejecucion es de forma aleatoria
class callThreads {
    public static void main(String[] args) {
        System.out.println("Begin");
        (new ReadInventory()).start();
        new Thread(new ThreadClass()).start();
        (new ReadInventory()).start();
        System.out.println("End");
    }
}

// Using run method -> No se comienza un nuevo hilo, la ejecucion es de forma secuencial
class callThreads2 {
    public static void main(String[] args) {
        System.out.println("Begin");
        (new ReadInventory()).run();
        new Thread(new ThreadClass()).run();
        (new ReadInventory()).run();
        System.out.println("End");
    }
}
