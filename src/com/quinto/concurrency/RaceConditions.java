package com.quinto.concurrency;

public class RaceConditions {
    public static void main(String[] args) {
        // Example: Create a user

        // 1 - Ambos usuarios pueden crear usuario con username ZooFan
        // Puede traer problemas para poder identificar a un user de otro

        // 2 - Ambos no pueden crear usuario con el mismo username y ambos obtiene error
        // Es parecido a un Livelock

        // 3 - Al menos uno si puede crear el usuario y los otros obtienen error
        // Es el mas usado, preserva la integridad de datos.
    }
}
