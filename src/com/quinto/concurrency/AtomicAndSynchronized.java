package com.quinto.concurrency;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

// Using Atomic classes
class SheepManager1 {
    private AtomicInteger counter = new AtomicInteger(0);

    private void increment() {
        System.out.print(counter.getAndIncrement() + " ");
    }

    public static void main(String[] args) {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(20);
            SheepManager1 manager = new SheepManager1();
            for (int i = 0; i < 10; i++) {
                service.submit(() -> manager.increment());
            }
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}

// SYNCHRONIZED - Cualquier objeto se puede sincronizar

// Using synchronized inside the FOR - Solo garantiza que los hilo de creen en orden
class SheepManager2 {
    private int counter = 0;

    private void increment() {
        System.out.print(++counter + " ");
    }

    public static void main(String[] args) {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(20);
            SheepManager2 manager = new SheepManager2();
            synchronized (manager) {
                for (int i = 0; i < 10; i++) {
                    service.submit(() -> manager.increment());
                }
            }
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}

// Using synchronized with an object - Garantiza que la salida sea ordenada
class SheepManager3 {
    private int counter = 0;

    private void increment() {
        synchronized (this){
            System.out.print(++counter + " ");
        }
    }

    public static void main(String[] args) {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(20);
            SheepManager3 manager = new SheepManager3();
            for (int i = 0; i < 10; i++) {
                service.submit(() -> manager.increment());
            }
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}

// Using synchronized in a method - Garantiza que la salida sea ordenada
class SheepManager4 {
    private int counter = 0;

    private synchronized void increment() {
            System.out.print(++counter + " ");
    }

    public static void main(String[] args) {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(20);
            SheepManager4 manager = new SheepManager4();
            for (int i = 0; i < 10; i++) {
                service.submit(() -> manager.increment());
            }
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}
