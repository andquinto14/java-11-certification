package com.quinto.concurrency.concurrencyApi.threadSafety;

// Para el examen
// 1. AtomicBoolean
// 2. AtomicInteger
// 3. AtomicLong

// METHODS
// get()
// set()
// getAndSet()
// incrementAndGet()
// getAndIncrement()
// decrementAndGet()
// getAndDecrement()

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

// ATOMIC CLASSES
class SheepManager1 {
    // Aunque es atomic no se garantiza el orden pero si la consistencia para que no hayan repetidos
    private AtomicInteger sheepCount = new AtomicInteger(0);

    private void incrementAndReport() {
        System.out.print((sheepCount.incrementAndGet()) + " ");
    }

    public static void main(String[] args) {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(20);
            SheepManager1 sheepManager = new SheepManager1();
            for (int i = 0; i < 10; i++) {
                service.submit(() -> sheepManager.incrementAndReport());
            }
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}

// SYNCHRONIZED
class SheepManager2 {
    // Aunque es atomic no se garantiza el orden pero si la consistencia para que no hayan repetidos
    private AtomicInteger sheepCount = new AtomicInteger(0);

    private void incrementAndReport() {
        System.out.print((sheepCount.incrementAndGet()) + " ");
    }

    public static void main(String[] args) {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(20);
            SheepManager2 sheepManager = new SheepManager2();
            for (int i = 0; i < 10; i++) {
                service.submit(() -> sheepManager.incrementAndReport());
            }
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}