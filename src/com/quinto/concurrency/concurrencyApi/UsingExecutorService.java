package com.quinto.concurrency.concurrencyApi;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

// IMPORTANTE
// 1. shutdown(), es void -> deja de recibir tareas, las que ya están se siguen ejecutando
// 2. shutdownNow(), devuelve una lista de Runnable -> mata todas la tareas incluso las que se están ejecutando
// 3. isShutdown()
// 4. isTerminated()

public class UsingExecutorService {
    public static void main(String[] args) {
        ExecutorService service = null;
        Runnable task1 = () -> {
            System.out.println("Printing inventory");
        };
        Runnable task2 = () -> {
            for (int i = 0; i < 3; i++) {
                System.out.println("Printing record: " + i);
            }
        };

        try {
            service = Executors.newSingleThreadExecutor(); // Garantiza que las tareas se ejecutarán de forma secuencial
            System.out.println("Begin");
            service.execute(task1);
            service.execute(task2);
            service.execute(task1);
            System.out.println("End");
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}

// SENDING TASKS - ExecutorService methods

// 1. void execute(Runnable command) -> Executes task at some point in the future
// 2. Future<?> submit(Runnable task) -> Executes task at some point in the future and returns
// 3. <T> Future<T> submit(Callable<T> task) -> Executes callable at some point in the future and returns
// 4. invokeAll(), espera por todas -> Puede arrojar exception
// 5. invokeAny(), espera solo una -> Puede arrojar exception

// WAITING FOR RESULTS - Future methods
// 1. boolean isDone()
// 2. boolean isCancelled()
// 3. boolean cancel(boolean mayInterruptIfRunning)
// 4. V get() -> Puede arrojar exception
// 5. V get(long timeout, TimeUnit unit) -> Puede arrojar exception

class CheckResults {
    private static int counter = 0;

    public static void main(String[] args) throws Exception {
        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            Future<?> result = service.submit(() -> {
                for (int i = 0; i < 500; i++) {
                    CheckResults.counter++;
                }
            });
            result.get(10, TimeUnit.SECONDS);
            System.out.println("Reached");
        } catch (TimeoutException e) {
            System.out.println("Not reached in time");
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}

// WAITING ALL TASKS TO FINISH
class Wait {
    public static void main(String[] args) throws Exception {
        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            Runnable task1 = () -> {
                System.out.println("Printing inventory");
            };
            Runnable task2 = () -> {
                for (int i = 0; i < 3; i++) {
                    System.out.println("Printing record: " + i);
                }
            };
            service.execute(task1);
            service.execute(task2);
        } finally {
            if (service != null)
                service.shutdown();
        }
        if (service != null) {
            service.awaitTermination(1, TimeUnit.MINUTES);
            if (service.isTerminated())
                System.out.println("Finished");
            else System.out.println("At least one task is still running");
        }
    }
}

// SENDING COLLECTION OF TASKS

// invokeAll
class Sending {
    public static void main(String[] args) throws Exception {
        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            System.out.println("Begin");
            Callable<String> task = () -> "result";
            List<Future<String>> list = service.invokeAll(List.of(task, task, task));
            for (Future<String> future : list) {
                System.out.println(future.get());
            }
            System.out.println("End");
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}

// invokeAll
class Sending2 {
    public static void main(String[] args) throws Exception {
        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            System.out.println("Begin");
            Callable<String> task = () -> "result";
            String data = service.invokeAny(List.of(task, task, task)); // apenas termina 1 cancela las otras
            System.out.println(data);
            System.out.println("End");
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}
