package com.quinto.concurrency.concurrencyApi;

// Like Runnable, is a functional interface
// V call() -> can throws an Exception

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class AddData {
    public static void main(String[] args) throws Exception{
        ExecutorService service = null;
        try {
            service = Executors.newSingleThreadExecutor();
            Future<Integer> result = service.submit(() -> 30 + 11); // using Callable
            System.out.println(result.get()); // 41
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}
