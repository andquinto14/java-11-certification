package com.quinto.concurrency.concurrencyApi;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

// METHODS
// 1. schedule(Callable<V> callable, long delay, TimeUnit unit)
// 2. schedule(Runnable command, long delay, TimeUnit unit)
// 3. scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit)
// 4. scheduleAtFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit)

public class SchedulingTasks {

    public static void main(String[] args) throws Exception{
        ScheduledExecutorService service = null;
        try {
            service = Executors.newSingleThreadScheduledExecutor();
            Runnable task1 = () -> System.out.println("Hello");
            Callable<String> task2 = () -> "Monkey";
            System.out.println("Begin");
            ScheduledFuture<?> r1 = service.schedule(task1, 10, TimeUnit.SECONDS);
            ScheduledFuture<?> r2 = service.schedule(task2, 8, TimeUnit.MINUTES);
            r1.get();
            r2.get();
            System.out.println("End");
            System.out.println("Available processors: " + Runtime.getRuntime().availableProcessors());
        }finally {
            if (service != null)
                service.shutdown();
        }
    }
}
