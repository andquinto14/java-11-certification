package com.quinto.concurrency;

// Se usa para orquestar tareas


import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// Without cyclicBarrier
class LionPenManager {
    private void removeLions() {
        System.out.println("Removing Lions");
    }

    private void cleanPen() {
        System.out.println("Cleaning the Pen");
    }

    private void addLions() {
        System.out.println("Adding Lions");
    }

    public void performTask() {
        removeLions();
        cleanPen();
        addLions();
    }

    public static void main(String[] args) {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(4);
            var manager = new LionPenManager();
            for (int i = 0; i < 4; i++) {
                service.submit(() -> manager.performTask());
            }
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}

// With cyclicBarrier
class LionPenManager2 {
    private void removeLions() {
        System.out.println("Removing Lions");
    }

    private void cleanPen() {
        System.out.println("Cleaning the Pen");
    }

    private void addLions() {
        System.out.println("Adding Lions");
    }

    public void performTask(CyclicBarrier barrier1, CyclicBarrier barrier2) {
        try {
            removeLions();
            barrier1.await();
            cleanPen();
            barrier2.await();
            addLions();
        } catch (BrokenBarrierException | InterruptedException e) {
        }
    }

    public static void main(String[] args) {
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(4); // si el numero de pool es diferente al de barrier se genera un deadlock
            var manager = new LionPenManager2();
            var c1 = new CyclicBarrier(4);
            var c2 = new CyclicBarrier(4, () -> System.out.println("*** Pen cleaned ***"));
            for (int i = 0; i < 4; i++) {
                service.submit(() -> manager.performTask(c1, c2));
            }
        } finally {
            if (service != null)
                service.shutdown();
        }
    }
}
