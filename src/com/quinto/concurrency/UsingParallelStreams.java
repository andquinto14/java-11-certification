package com.quinto.concurrency;

import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.stream.Stream;

public class UsingParallelStreams {
    public static void main(String[] args) {

        // stream() es una operacion intermedia, opera sobre el stream

        // Opcion 1
        Stream<Integer> s1 = List.of(1, 2).stream();
        Stream<Integer> s2 = s1.parallel();

        // Opcion 2
        Stream<Integer> s3 = List.of(1, 2).parallelStream();
    }
}

// Ejecutando parallel decomposition
class ParallelDecomposition {
    public static void main(String[] args) {

        // With stream() only
        long start = System.currentTimeMillis();
        Stream.of(1, 2, 3, 4, 5)
                .map(ParallelDecomposition::doWork)
                .forEach(s -> System.out.print(s + " "));
        System.out.println();
        var timeTaken = (System.currentTimeMillis() - start) / 1000;
        System.out.println("Time: " + timeTaken);
        System.out.println("----------------------------");

        // With parallelStream()
        // perdemos el orden
        // pero con forEachOrdered obtenemos orden, aunque disminuye el perfomance.
        long start2 = System.currentTimeMillis();
        List.of(1, 2, 3, 4, 5)
                .parallelStream()
                .map(ParallelDecomposition::doWork)
                .forEachOrdered(s -> System.out.print(s + " "));
        System.out.println();
        var timeTaken2 = (System.currentTimeMillis() - start2) / 1000;
        System.out.println("Time: " + timeTaken2);
    }

    public static int doWork(int input) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return input;
    }
}

// Tareas basadas en orden
class OrderTasks {
    public static void main(String[] args) {
        System.out.println(List.of(1, 3, 4, 5, 6)
                .stream().findAny().get());

        System.out.println("----------------------");

        System.out.println(List.of(1, 3, 4, 5, 6)
                .parallelStream().findAny().get()); // la maquina virtual crea
        // un numero de hilos para procesar el stream y escoje cualquiera de esos.
        // El resultado es impredecible
    }
}

// Creando streams no ordenados
class NoOrdered {
    public static void main(String[] args) {
        List.of(1, 2, 3, 4, 5).stream().unordered();

        // En este caso no tiene efecto porque los parallel son unordered
        List.of(1, 2, 3, 4, 5).stream().unordered().parallel();

    }
}

// Combinando resultados con reduce()
// <U> U reduce (U identity, BiFunction<U, ? super T,U> accumularor, BinaryOperator<U> combiner
class UsingReduce {
    public static void main(String[] args) {
        System.out.println(List.of('w', 'o', 'l', 'f')
                .parallelStream()
                .reduce("", (s1, c) -> s1 + c, (s2, s3) -> s2 + s3));
        System.out.println("------------------------");

        System.out.println(List.of("w", "o", "l", "f")
                .parallelStream()
                .reduce("X", String::concat)); // XwXoXlXf
        System.out.println("------------------------");

        // Problematic accumulator
        System.out.println(
                List.of(1,2,3,4,5,6)
                        .parallelStream()
                        .reduce(0, (a, b) -> (a-b))
        ); // resultado impredecible

    }
}

// Combinando resultados con collect()
// <R> R collect(Supplier<R> supplier, BiConsumer<R, ? super T> accumulator, BiConsumer<R, R> combiner)

class UsingCollect{
    public static void main(String[] args) {
        // REQUISITOS
        // 1. El stream debe ser parallel
        // 2. El parameter de la operacion collect() tiene una caracteristica Characteristics.CONCURRENCY
        // 3. Sea que el stream esta no ordenado o el collector tenga caracteristica Characteristics.UNORDERED

        Stream<String> stream = List.of("w", "o", "l", "f")
                .parallelStream();
        SortedSet<String> set = stream.collect(ConcurrentSkipListSet::new,
                Set::add, Set::addAll);
        System.out.println(set); // [f,l,o,w]
    }
}