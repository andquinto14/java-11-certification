package com.quinto.concurrency;

public class RunnableInterface {
    public static void main(String[] args) {
        Runnable sloth = () -> System.out.println("Hello word");
        sloth.run();
    }
}

class CalculateAverage implements Runnable{
    private double[] scores;
    public CalculateAverage(double[] scores) {
        this.scores = scores;
    }

    @Override
    public void run() {
        System.out.println(scores);
    }
}
