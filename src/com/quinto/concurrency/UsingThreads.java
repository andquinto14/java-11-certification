package com.quinto.concurrency;

public class UsingThreads {

}

// Pooling with Sleep -> cuando un hijo necesita esperar el resultado de otr
class CheckResults {
    private static int counter = 0;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            for (int i = 0; i < 500; i++) {
                CheckResults.counter++;
            }
        }).start();
        while (CheckResults.counter < 100){
            System.out.println("Not reached yet");
            Thread.sleep(1000); // Se aplica polling sleep, el hilo espera 1s
        }
        System.out.println("Reached");
    }
}
