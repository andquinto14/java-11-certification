package com.quinto.concurrency;

// concurrentHashMap
// concurrentLinkedQueue
// concurrentListMap
// concurrentSkipListMap
// concurrentSkipListSet
// copyOnWriteArrayList -> copia elementos a nueva estructura cada vez que se elimina, agrega o modifica(si se cambia la referencia) un elemento, la referencia principal apunta a la nueva copia
// copyOnWriteArraySet
// LinkedBlockingQueue

import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.*;
import java.util.stream.Collectors;

class UsingConcurrentCollections {
    public static void main(String[] args) {
        // concurrentHashMap
        var foodData = new ConcurrentHashMap<String, Integer>();
        foodData.put("penguin", 1);
        foodData.put("flamingo", 2);
        System.out.println(foodData.get("flamingo")); //2
        System.out.println("--------------------------");

        // concurrentLinkedQueue
        Queue<Integer> queue = new ConcurrentLinkedDeque<>();
        queue.offer(31);
        System.out.println(queue.peek()); // 31
        System.out.println(queue.poll()); // 31
        System.out.println("--------------------------");

        // concurrentListMap
        Map<String, String> rainForestAnimalDiet = new ConcurrentSkipListMap<>();
        rainForestAnimalDiet.put("koala", "bamboo");
        rainForestAnimalDiet.put("pelican", "orna");
        rainForestAnimalDiet.entrySet()
                .stream()
                .forEach((e) -> System.out.println(e.getKey() + "-" + e.getValue())); // koala-bamboo
        System.out.println("--------------------------");

        // concurrentSkipListSet
        Set<String> gardenAnimals = new ConcurrentSkipListSet<>();
        gardenAnimals.add("Rabbit");
        gardenAnimals.add("Gopher");
        System.out.println(gardenAnimals.stream().collect(Collectors.joining(","))); // Gopher,Rabbit
        System.out.println("--------------------------");

        // copyOnWriteArrayList
        List<Integer> favNumbers = new CopyOnWriteArrayList<>(List.of(4, 3, 42));
        for (var n : favNumbers) {
            System.out.print(n + " ");
            favNumbers.add(9);
        }
        System.out.println();
        System.out.println("Size: " + favNumbers.size());
        System.out.println("--------------------------");

        // copyOnWriteArraySet
        Set<Character> favLetters = new CopyOnWriteArraySet<>(List.of('a', 't'));
        for (char c : favLetters) {
            System.out.print(c + " ");
            favLetters.add('s');
        }
        System.out.println();
        System.out.println("Size: " + favLetters.size()); // 6, set don´t allow duplicated values
        System.out.println("--------------------------");

        // Deleting with loop
        List<String> birds = new CopyOnWriteArrayList<>();
        birds.add("hawk");
        birds.add("hawk");
        birds.add("hawk");
        for (var bird : birds) {
            birds.remove(bird);
        }
        System.out.println(birds.size()); // 0
        System.out.println("--------------------------");

        // LinkedBlockingQueue
        try {
            var blockingQueue = new LinkedBlockingQueue<Integer>();
            blockingQueue.offer(39);
            blockingQueue.offer(3, 4, TimeUnit.SECONDS);
            System.out.println(blockingQueue.poll()); // 39
            System.out.println(blockingQueue.poll(10, TimeUnit.MILLISECONDS)); // 3
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
