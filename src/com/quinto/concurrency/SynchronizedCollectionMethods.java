package com.quinto.concurrency;

// synchronizedCollection(Collection<T> c)
// synchronizedList(List<T> list)
// synchronizedMap(Map<K,V> m)
// synchronizedNavigableMap(NavigableMap<K,V> m)
// synchronizedNavigableSet(NavigableSet<T> s)
// synchronizedSet(Set<T> s)
// synchronizedSortedMap(SortedMap<K,V> m)
// synchronizedSortedSet(SortedSet<T> s)

import java.util.Collections;
import java.util.HashMap;

public class SynchronizedCollectionMethods {
    public static void main(String[] args) {
        // synchronizedMap
        var foodData = new HashMap<String, Integer>();
        foodData.put("penguin", 1);
        foodData.put("flamingo", 2);
        var syncFoodData = Collections.synchronizedMap(foodData);
        for (var key : syncFoodData.keySet()){
            syncFoodData.remove(key); // ConcurrentModificationException
        }

    }
}
