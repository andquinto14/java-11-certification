package com.quinto.concurrency;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class Cache {

    static ConcurrentHashMap<String, Object> chm
            = new ConcurrentHashMap<String, Object>();

    public static void main(String[] args) {
        chm.put("a", "aaa");
        chm.put("b", "bbb");
        chm.put("c", "ccc");

        new Thread() {
            public void run() {
                Iterator<Map.Entry<String, Object>> it = Cache.chm.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, Object> en = it.next();
                    if (en.getKey().equals("a") || en.getKey().equals("b")) {
                        it.remove();
                    }
                }
            }
        }.start();

        new Thread() {
            public void run() {
                Iterator<Map.Entry<String, Object>> it = Cache.chm.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, Object> en = it.next();
                    System.out.print(en.getKey() + ", ");
                }
            }
        }.start();
    }
}

class Test extends Thread {
    static Object obj1 = new Object();
    static Object obj2 = new Object();

    public void m1() {
        synchronized (obj1) {
            System.out.print("1 ");
            synchronized (obj2) {
                System.out.println("2");
            }
        }
    }

    public void m2() {
        synchronized (obj2) {
            System.out.print("2 ");
            synchronized (obj1) {
                System.out.println("1");
            }
        }
    }

    public void run() {
        m1();
        m2();
    }

    public static void main(String[] args) {
        new Test().start();
        new Test().start();
    }
}

class Transfer implements Runnable {
    Account from, to;
    double amount;

    public Transfer(Account from, Account to, double amount) {
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public void run() {
        synchronized (from) {
            from.setBalance(from.getBalance() - amount);
            synchronized (to) {
                to.setBalance(to.getBalance() + amount);
            }
        }
    }
}

class Account {
    private String id;
    private double balance;
    //constructor and accessor methods not shown,

    public Account(String id, double balance) {
        this.id = id;
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public double getBalance() {
        return balance;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}

class Test1 {
    public static void main(String[] args) {
        var es = Executors.newCachedThreadPool();
        var a1 = new Account("A1", 1000);
        var a2 = new Account("A1", 1000);
        es.submit(new Transfer(a1, a2, 200));
        es.submit(new Transfer(a2, a1, 300));
    }
}

class Student {

    private Map<String, Integer> marksObtained
            = new HashMap<>();
    private ReadWriteLock lock = new ReentrantReadWriteLock();

    public void setMarksInSubject(String subject, Integer marks) {
        lock.writeLock().lock(); //1
        try {
            marksObtained.put(subject, marks);
        } finally {
            lock.writeLock().unlock(); //2
        }
    }

    public double getAverageMarks() {
        lock.readLock().lock(); //3
        double sum = 0.0;
        try {
            for (Integer mark : marksObtained.values()) {
                sum = sum + mark;
            }
            return sum / marksObtained.size();
        } finally {
            lock.readLock().unlock();//4
        }
    }

    public static void main(String[] args) {

        final Student s = new Student();

        //create one thread that keeps adding marks
        new Thread() {
            public void run() {
                int x = 0;
                while (true) {
                    int m = (int) (Math.random() * 100);
                    s.setMarksInSubject("Sub " + x, m);
                    x++;
                }
            }
        }.start();

        //create 5 threads that get average marks
        for (int i = 0; i < 5; i++) {
            new Thread() {
                public void run() {
                    while (true) {
                        double av = s.getAverageMarks();
                        System.out.println(av);
                    }
                }
            }.start();
        }
    }

}

class MyCallable implements Callable<String> {
    public String call() throws Exception {
        Thread.sleep(5000);
        return "DONE";
    }
}

class TestClass {

    public static void main(String[] args) throws Exception {
        var es = Executors.newSingleThreadExecutor();
        var future = es.submit(new MyCallable());
        System.out.println(future.get()); //1 - print DONE after 5 seconds
        es.shutdownNow(); //2 -
    }
}