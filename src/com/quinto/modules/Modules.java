package com.quinto.modules;
// BENEFICIOS
// 1. Mejoran el control de acceso
// 2. Administracion clara de dependencias
// 3. Java build personalizado
// 4. Mejora el performance
// 5. Reforzar el unique package

public class Modules {
    // Crear modulo
    // javac -p mods -d feeding feeding/animal/feeding/Task feeding/*.java

    //Ejecutar medulos
    // java --module-path feeding --module zoo.animal.feeding/zoo.animal.feeding.Task

    // Empaquetar modulo
    // jar -cvf mods/zoo.animal.feeding.jar -C feeding/ .

    // El paquete a importar debe tener una clase .java

    // exports and requires
    /*module zoo.animal.feeding{
        exports zoo.animal.feeding;
        imports zoo.animal.media;
    }*/

    // exports se usa para exportar un paquete a otros modulos
    // Es posible tambien exportar un paquete a un modulo especifico
    /*module zoo.animal.feeding{
        exports zoo.animal.feeding to zoo.animal.staff;
    }*/

    // requires transitive
    // 1. requires moduleName, esto signinica que el modulo actual depende del moduleName
    // 2. requires transitive moduleName, significa que cualquier modulo que requiera a este modulo
    //    tambien dependerá del moduleName

    // No se pueden duplicar requires, asi sean transitive

    // El keyword "provides" especifica que una clase provee una implementacion de un servicio.
    // provides zoo.staff.ZooApi with zoo.staff.ZooImpl

    // El keyword "uses" especifica que un modulo depende de un servicio
    // uses zoo.staff.ZooApi

    // Describir un modulo
    // java -p mods -d zoo.animal.feeding or java -p mods --describe-module zoo.animal.feeding

    // jdeps - Este comando da informacion acerca de las dependencias en un modulo
    // jdeps -s mods/zoo.animal.feeding.jar

    // jmod - Solo se una con archivos jmod, son recomendados cuando tienes librerias
    // nativas o algo que no puede ir dentro de un jar.


}
