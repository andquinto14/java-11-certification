package com.quinto.DataTypes;

import java.util.ArrayList;

public class DataTypes {
    public static void main(String[] args) {
        // [-127, 127]
        // default value is 0
        // can also be used in place of "int"
        byte b = 127;
        byte b2 = '1';

        // [-32,768, 32,767]
        // default value is 0
        // is 2 times smaller than an integer.
        short sh = 32767;
        short sh2 = '1';

        // [-2,147,483,648, 2,147,483,647]
        // default value is 0
        int in = 12;

        // [-9,223,372,036,854,775,808, 9,223,372,036,854,775,807]
        // default value is 0
        long l = 100000L;
        long l2 = -200000L;

        //Its value range is unlimited
        float f1 = 234.5f;
        float ft = -121;

        //Its value range is unlimited
        double d1 = 12.357457;
        double db = 12;

        //Its value-range lies between '\u0000' (or 0) to '\uffff' (or 65,535 inclusive)
        char letterA = 'A';
        char ch = 12345;

        String st = "12";
        boolean bl = false;

        var al = new ArrayList<String>();
        al.forEach( k -> {
            System.out.print(k.length());
        });

    }
}
