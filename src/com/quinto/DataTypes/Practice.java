package com.quinto.DataTypes;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.List;

/*import java.util.ArrayList;

class Data {
    private int x = 0;
    private String y = "Y";
    public Data(int k){
        this.x = k;
    }
    public Data(String k){
        this.y = k;
    }
    public void showMe(){
        System.out.println(x+y);
    }
}

public class Practice {

    static String str = "Hello World";
    public static void changeIt(String s){
        s = "Good bye world";
    }

    static boolean a;
    static boolean bb;
    static boolean c;

    public static void main(String[] args) {
        int i1 = 1, i2 = 2, i3 = 3;
        int i4 = i1 + (i2 = i3);
        System.out.println(i4);
        // x1 = -3, x2 = -4, x3 = -4,
        String s1 = "String".replace('g', 'G');
        String s2 = "String".replace('g', 'G');
        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
        System.out.println("------------------------------------");
        new Data(10).showMe();
        new Data("Z").showMe();
        System.out.println("------------------------------------");
        changeIt(str);
        System.out.println(str);
        System.out.println("------------------------------------");
        byte b = -128;
        int i = b;
        b = (byte) i;
        System.out.println(i + " " + b);
        boolean bf = true || false;
        boolean db = true | false;
        var bv = 10 > 11;

        // x = 2, y = 2, z = 1
        // a = 1, y = 1, b = 1, z = 0
        // b = 2, z = 1
        System.out.println(7 / 2);

        System.out.println("------------------------------------");
        boolean bool = (a = true) || (bb = true) && (c = true);
        System.out.print(a + ", " + bb + ", " + c);

        System.out.println("------------------------------------");
        byte bbb = 1;
        char c = 1;
        short s = 1;
        int ii = 1;
        *//*s = bbb * bbb;
        i = bbb + bbb;
        s *= bbb;
        c = c + b;
        s += ii;*//*

        String str = "10";

        int iVal = 0;
        Double dVal = 0.0;
        try {
            iVal = Integer.parseInt(str, 2);  //1
            if ((dVal = Double.parseDouble(str)) == iVal) { //2
                System.out.println("Equal");
            }
        } catch (NumberFormatException e) {
            System.out.println("Exception in parsing");
        }
        System.out.println(iVal + " " + dVal);
        System.out.println("------------------------------------");

        var x = 1____3;   //1

        var y = 1_3;     //2

        var z = 3.234_567f; //3

        System.out.println(x+" "+y+" "+z);

        System.out.println("------------------------------------");
        float foo = 2, bar = 3, baz = 4;    //1
        float mod1 = foo % baz, mod2 = baz % foo;  //2
        System.out.println(mod1);
        System.out.println(mod2);
        float val = mod1>mod2? bar : baz; //3
        System.out.println(val);

        int yi, j, k;
        yi = j = k = 9;
        System.out.println(yi);
        System.out.println(j);

        System.out.println("    hello java guru   ".strip());

        *//*var i[][]  = { { 1, 2 }, { 1 }, { }, { 1, 2, 3 } } ;
        var ig[ ] = new int[2] {1, 2} ;
        var ibbv = new int[ ][ ] { {1, 2, 3}, {4, 5, 6} } ;
        var irf[4] = new int[]{ 1, 2, 3, 4 } ;
        var fa = new Float{ 1.1F, 2.2F, 3.3F };

        char c;
        int i;
        c = 'a';//1
        i = c;  //2
        i++;    //3
        c = i;  //4
        c++;    //5*//*
        System.out.println("------------------------------------");
        String examName = "OCP Java 11";
        String uniqueExamName = new String(examName);
        String internedExamName = uniqueExamName.intern();
        System.out.println(
                (examName==uniqueExamName)+" "+
                        (examName == internedExamName)+" "+
                        (uniqueExamName == internedExamName) );

        System.out.println("------------------------------------");
        System.out.println(!"".isBlank());

        *//*double dggg = 10,000,000.0;
        double dss = 10-000-000;
        double ddd = 10_000_000;
        double dff = 10 000 000;
*//*
        *//*var values = new ArrayList<String>();
        values.sort(String::compareTo);
        String[] sa = values.toArray();

        float f1 = 1.0;
        float f = 43e1;
        float fs = 0x0123;
        float af = 4;
        var fss = 4f;
        StringBuilder stringBuilder = new StringBuilder("afasf");
        stringBuilder.ap*//*

        String fullPhoneNumber = "333-333-3333";
        System.out.println(new StringBuilder(fullPhoneNumber).substring(0, 8)+"xxxx");
        System.out.println(new StringBuilder(fullPhoneNumber).replace(8, 12, "xxxx").toString());
        System.out.println(new StringBuilder("xxxx").append(fullPhoneNumber, 0, 8).toString());
        System.out.println(new StringBuilder("xxxx").insert(0, fullPhoneNumber, 0, 8).toString());
        System.out.println(new StringBuilder(fullPhoneNumber).append("xxxx", 8, 12).toString());

        *//*int a;
        int b = 0;
        int c;
        public void m(){
            int d;
            int e = 0;
            // Line 1
        }*//*

    }
}*/
/*class Test{ }  // 1

@Override
class TestClass {
    public static void main(String[] args)  { // 3

        int i = 5;
        float f = 5.5f;
        double d = 3.8;
        char c = 'a';
        if (i == f) c++;
        if (((int) (f + d)) == ((int) f + (int) d)) c += 2;
        System.out.println(107%9.1);
        Object t = new Integer(107);
        var values = new ArrayList<String>();
        values.forEach( k -> System.out.print(k.length()));
        values.forEach( (var k)->System.out.print(k.length()));
        for(var value : values){    }
        for(var x : System.getProperties().keySet()){
            System.out.println(x.length());
        }
        float ff = 0x0123;
        var df = 4f;
    }
}*/
/*
class TestClass{
    String global = "111";

    public int parse(String arg){
        var value = 0;
        try{
            String global = arg;
            global.su
            value = Integer.parseInt(global);
        }
        catch(Exception e){
            System.out.println(e.getClass());
        }
        System.out.print(global+" "+value+" ");
        return value;
    }
    public static void main(String[] args) {
        System.out.println("String".replace('g','G')=="StrinG");
        System.out.println("String".replace('g','g')=="String");
        System.out.println("String".replace('g','g') == new String("String").replace('g','g'));
    }
}*/
/*
class TestClass {

    public static int m1(int i){
        return ++i;
    }

    public static void main( var args[] ) {

        int k = m1(args.length);
        k += 3 + ++k;
        System.out.println(k);
    }

}*/
/*
class X{
    int val = 10;
}

class Y extends X{
    Y val = null; //1
}

class TestClass extends X{

    public static void main(String[] args){
        float foo = 2, bar = 3, baz = 4;    //1
        float mod1 = foo % baz, mod2 = baz % foo;  //2
        float val = mod1>mod2? bar : baz; //3
        System.out.println(val);
    }
}*/

@Retention(value= RetentionPolicy.RUNTIME)
@Target(value={ElementType.TYPE_USE, ElementType.TYPE_PARAMETER})
@interface NonNull{
}

class TestClass {
    public  void myMethod(String... params)  {
        var a  = params;
        var b = params[0];
    }

    static void doElements(List l) {
        l.add("string"); //1
        System.out.println(l.get(0)); //2
    }

    public static void main(String[] args) {
        String str1 = "str1";
        String str2 = "str2";
        System.out.println( str1.concat(str2) );
        System.out.println(str1);
        System.out.println("Hello world".toLowerCase( ).equals("hello world"));
        int x = 1;
        int y = 2;
        int z = x++;
        int a = --y;
        int b = z--;
        b += ++z;

        int answ = x>a?y>b?y:b:x>z?x:z;
        System.out.println(answ);
    }
}