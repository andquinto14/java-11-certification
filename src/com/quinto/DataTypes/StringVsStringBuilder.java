package com.quinto.DataTypes;

import java.util.ArrayList;
import java.util.Arrays;

public class StringVsStringBuilder {
    public static void main(String[] args) {
        System.out.println("--------------STRING------------------");
        //String objects are immutables
        //String pool is a separated memory inside the heap memory where literals are stored
        String name = "Andres";
        String name2 = "Andres";
        System.out.println("--------------------------------");

        //Both variables will print the same reference or hashcode
        System.out.println(name.hashCode());
        System.out.println(name2.hashCode());
        System.out.println("--------------------------------");

        //When we change a String it changes the reference because of immutability
        String a = "aaa";
        System.out.println(a.hashCode());
        a = "bbb";
        System.out.println(a.hashCode());
        System.out.println("--------------------------------");

        // == is a comparator and compares the value and the hashcode
        // .equals is a method and compares only the values
        String one = "one";
        String two = "one";
        System.out.println(one == two);
        System.out.println("--------------------------------");

        //Create different objects with the same value
        String ob1 = new String("me");
        String ob2 = new String("me");
        System.out.println(ob1.hashCode());
        System.out.println(ob2.hashCode());
        System.out.println(ob1 == ob2);
        System.out.println(ob1.equals(ob2));
        System.out.println("--------------------------------");

        // toString method
        int[] numbers = new int[]{1, 2, 3};
        System.out.println(numbers);
        System.out.println(Arrays.toString(numbers));
        Integer num = new Integer(2);
        System.out.println(num.toString());
        System.out.println(num.hashCode());
        System.out.println("--------------------------------");

        // String concatenation
        // addition with characters converts it into ascii value
        System.out.println('a' + 'b');
        System.out.println("a" + "b");
        System.out.println((char) ('a' + 3));
        // integer will be converted to Integer that will call toString()
        System.out.println(("a" + 1));
        System.out.println("Andres" + new ArrayList<>());
        System.out.println("Andres" + new Integer(56));
        // + in java only can be use with primitives
        System.out.println(new Integer(56) + "" + new ArrayList<>());
        // compile error
        // System.out.println(new Integer(56) + new ArrayList<>());

        // String Performance
        String series = "";
        for (int i = 0; i < 26; i++) {
            char ch = (char) ('a' + i);
            series = series + ch;
        }
        System.out.println(series);

        System.out.println("--------------STRING BUILDER------------------");
        // StringBuilder is mutable
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 26; i++) {
            char ch = (char) ('a' + i);
            stringBuilder.append(ch);
        }
        System.out.println(stringBuilder);
        stringBuilder.reverse();
        System.out.println(stringBuilder);

        System.out.println("--------------METHODS------------------");
        // Methods
        String name1 = "Andres Quinto";
        System.out.println(Arrays.toString(name1.toCharArray()));
        System.out.println(name1.toLowerCase());
        System.out.println(name1);
        System.out.println(name1.indexOf('A'));
        System.out.println("   Andres ".strip());
        System.out.println(Arrays.toString(name1.split(" ")));
    }
}
