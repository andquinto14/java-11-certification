package com.quinto.DataTypes;

public class StringBuilderOperations {
    public static void main(String[] args) {
        // StringBuilder() - It creates an empty String Builder with the initial capacity of 16.
        // StringBuilder(String str) - It creates a String Builder with the specified string.
        // StringBuilder(int length) - It creates an empty String Builder with the specified capacity as length.

        // append() method
        // The StringBuilder append() method concatenates the given argument with this String.
        StringBuilder sb = new StringBuilder("Hello ");
        sb.append("Java");//now original string is changed
        System.out.println(sb);//prints Hello Java
        System.out.println("--------------------------------");

        // insert() method
        // The StringBuilder insert() method inserts the given string with this string at the given position.
        StringBuilder sb1 = new StringBuilder("Hello ");
        sb1.insert(1, "Java");//now original string is changed
        System.out.println(sb1);//prints HJavaello
        System.out.println("--------------------------------");

        // replace() method
        // The StringBuilder replace() method replaces the given string from the specified beginIndex and endIndex.
        StringBuilder sb3 = new StringBuilder("Hello");
        sb3.replace(1, 3, "Java");
        System.out.println(sb3);//prints HJavalo
        System.out.println("--------------------------------");

        // delete() method
        // The delete() method of StringBuilder class deletes the string from the specified beginIndex to endIndex.
        StringBuilder sb4 = new StringBuilder("Hello");
        sb4.delete(1, 3);
        System.out.println(sb4);//prints Hlo
        System.out.println("--------------------------------");

        // reverse() method
        // The reverse() method of StringBuilder class reverses the current string.
        StringBuilder sb5 = new StringBuilder("Hello");
        sb5.reverse();
        System.out.println(sb5);//prints olleH
        System.out.println("--------------------------------");

        // capacity() method
        // The capacity() method of StringBuilder class returns the current capacity of the Builder.
        // The default capacity of the Builder is 16.
        StringBuilder sb6 = new StringBuilder();
        System.out.println(sb6.capacity());//default 16
        sb6.append("Hello");
        System.out.println(sb6.capacity());//now 16
        sb6.append("Java is my favourite language");
        System.out.println(sb6.capacity());//now (16*2)+2=34 i.e (oldcapacity*2)+2
        System.out.println("--------------------------------");

        // ensureCapacity() method
        // The ensureCapacity() method of StringBuilder class ensures that the given capacity is the minimum to the current capacity.
        StringBuilder sb7 = new StringBuilder();
        System.out.println(sb7.capacity());//default 16
        sb7.append("Hello");
        System.out.println(sb7.capacity());//now 16
        sb7.append("Java is my favourite language");
        System.out.println(sb7.capacity());//now (16*2)+2=34 i.e (oldcapacity*2)+2
        sb7.ensureCapacity(10);//now no change
        System.out.println(sb7.capacity());//now 34
        sb7.ensureCapacity(50);//now (34*2)+2
        System.out.println(sb7.capacity());//now 70
        System.out.println("--------------------------------");
    }
}
