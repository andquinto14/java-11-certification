package com.quinto.DataTypes;

public class WrapperClasses {
    public static void main(String[] args) {
        // Wrapper class are predefined class by ava which can contain primitive data type
        // boolean - Boolean
        // byte - Byte
        // char - Char
        // float - Float
        // int - Integer
        // long - Long
        // short - Short
        // double - Double
        int a = 100;
        System.out.println("Primitive: " + a);
        Integer b = 100;
        System.out.println("Wrapper: " + b);
        System.out.println("--------------------------------");

        // Parsing from String to respective data type
        String price = "463";
        String price1 = "237";
        System.out.println(price + price1);
        //int result = price + price1; does not compile
        int p = Integer.parseInt(price);
        int p1 = Integer.parseInt(price1);
        System.out.println(p + p1);
        System.out.println("--------------------------------");

        // Boxing and unboxing
        // Autoboxing
        int in = 90;
        Integer in1 = a;
        System.out.println("Autoboxing: " + in1);

        // Boxing
        int x = 70;
        Integer y = Integer.valueOf(x);
        System.out.println("Boxing: " + y);

        // Unboxing
        Integer result = 102;
        int newResult = result.intValue();
        System.out.println("Unboxing: " + newResult);

        // AutoUnboxing
        Integer marks = 78;
        int newMarks = marks;
        System.out.println("AutoUnboxing: " + newMarks);
    }
}
