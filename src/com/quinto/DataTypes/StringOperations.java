package com.quinto.DataTypes;

import java.util.Arrays;

public class StringOperations {
    public static void main(String[] args) {
        char[] ch = {'j', 'a', 'v', 'a', 't', 'p', 'o', 'i', 'n', 't'};
        String s = new String(ch);
        System.out.println(s);
        System.out.println(s.intern());
        System.out.println("--------------------------------");

        // Immutable
        String st = "Sachin";
        st.concat(" Tendulkar");//concat() method appends the string at the end
        System.out.println(st);//will print Sachin because strings are immutable objects
        System.out.println("--------------------------------");

        // If we explicitly assign it to the reference variable, it will refer to "Sachin Tendulkar" object.
        String st2 = "Sachin";
        st2 = st2.concat(" Tendulkar");
        System.out.println(st2);
        System.out.println("--------------------------------");

        // Comparing Strings with .equals() method
        // public boolean equals(Object another) compares this string to the specified object.
        // public boolean equalsIgnoreCase(String another) compares this string to another string, ignoring case.
        String s1 = "Sachin";
        String s2 = "Sachin";
        String s3 = new String("Sachin");
        String s4 = "Saurav";
        System.out.println(s1.equals(s2));//true
        System.out.println(s1.equals(s3));//true
        System.out.println(s1.equals(s4));//false
        System.out.println("--------------------------------");

        // Comparing Strings with ==
        // The == operator compares references not values.
        String s11 = "Sachin";
        String s22 = "Sachin";
        String s33 = new String("Sachin");
        System.out.println(s11 == s22);//true (because both refer to same instance)
        System.out.println(s11 == s33);//false(because s3 refers to instance created in nonpool)
        System.out.println("--------------------------------");

        // Comparing Strings with compareTo() method
        // compareTo() method compares values lexicographically and returns an integer value that describes if first string is less than, equal to or greater than second string.
        // Suppose s1 and s2 are two String objects
        // s1 == s2 : The method returns 0.
        // s1 > s2 : The method returns a positive value.
        // s1 < s2 : The method returns a negative value.
        String s111 = "Sachin";
        String s222 = "Sachin";
        String s333 = "Ratan";
        System.out.println(s111.compareTo(s222));//0
        System.out.println(s111.compareTo(s333));//1(because s1>s3)
        System.out.println(s333.compareTo(s111));//-1(because s3 < s1 )
        System.out.println("--------------------------------");

        // SubString
        // Index starts from 0.
        // public String substring(int startIndex)
        // public String substring(int startIndex, int endIndex)
        // startIndex: inclusive, endIndex: exclusive
        String stt = "hello";
        System.out.println(stt.substring(0, 2)); //returns he  as a substring
        String ss = "SachinTendulkar";
        System.out.println("Original String: " + ss);
        System.out.println("Substring starting from index 6: " + ss.substring(6));//Tendulkar
        System.out.println("Substring starting from index 0 to 6: " + ss.substring(0, 6)); //Sachin
        System.out.println("--------------------------------");

        // .split() method
        // The split() method of String class can be used to extract a substring from a sentence
        String text = new String("Hello, My name is Sachin");
        /* Splits the sentence by the delimeter passed as an argument */
        String[] sentences = text.split("\\.");
        System.out.println(Arrays.toString(sentences));
        System.out.println("--------------------------------");

        // trim() method
        // The String class trim() method eliminates white spaces before and after the String.
        String sst = "  Sachin  ";
        System.out.println(sst);//  Sachin
        System.out.println(sst.trim());//Sachin
        System.out.println("--------------------------------");

        // charAt() Method
        // The String class charAt() method returns a character at specified index.
        String tt = "Sachin";
        System.out.println(tt.charAt(0));//S
        System.out.println(tt.charAt(3));//h
        System.out.println("--------------------------------");

        // valueOf() Method
        // The String class valueOf() method coverts given type such as int, long, float, double, boolean, char and char array into String.
        int a = 10;
        String rs = String.valueOf(a);
        System.out.println(rs + 10);

    }
}
