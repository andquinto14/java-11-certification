package com.quinto.JavaObjectOriented.Enums;

import java.util.List;

/*
import java.util.ArrayList;
import java.util.Set;
*/
/*


import java.lang.annotation.Repeatable;

public class practice {
    public @interface Authors {
        Author[] value();
        String team() default "enthuware";
    }

    @Repeatable(Authors.class)
    public @interface Author {
        int id() default 0;
        String[] value();
    }

    @Authors(value=@Author("bob"), team="java")
    @Author("alice")
    void someMethod(int index){
    }

    @Author(value="alice", team="networking")
    class TestClass{
    }

    public static void main(String[] args) {
        @Authors(value=@Author("bob"), team="java")
        String value;
    }
}
*//*

*/
/*
@Target({ElementType.LOCAL_VARIABLE, ElementType.FIELD, ElementType.METHOD}) //1
@Target({ElementType.TYPE})//2
@Retention(RetentionPolicy.RUNTIME)//3
@SafeVarargs
public @interface DebugInfo { //4
    String name() default=""; //5
    String[][] params();//6
    java.util.Date entryTime();//7
}*//*

public class practice {
    int i;
    public practice(int i) { this.i = i;  }
    public String toString(){
        if(i == 0) return null;
        else return ""+i;
    }
    public static void main(String[] args) {
        int i = 5;
        float f = 5.5f;
        double d = 3.8;
        char c = 'a';
        if (i == f) c++;
        if (((int) (f + d)) == ((int) f + (int) d)) c += 2;
        System.out.println(c);
        StringBuilder d = new StringBuilder();
        String s = "gg";
        s += "hh";
        int[] a = { 1 };
        char b = 98;
        var _ = new ArrayList<>();
        var i[4] = new int[]{ 1, 2, 3, 4 } ;
        var fa = new Float{ 1.1F, 2.2F, 3.3F };
    }
}*/
/*
@Target(value={ElementType.TYPE_USE,ElementType.TYPE_PARAMETER})
@interface Interned{
}


class Account{
}

class x {
    public static void main(String[] args) {
        var str = "Hello"+ (@Interned "World");
        @Interned Account acct = new Account();
        Account acctc = new @Interned Account();
    }
}*/
/*
@Retention(value=RetentionPolicy.RUNTIME)
@Target(value={ElementType.TYPE_USE,ElementType.TYPE_PARAMETER})
@SafeVarargs()
@interface NonNull{

}

class m{
    @NonNull String str = "";
    String strd = (@NonNull String) "";

}*/
/*
class Account {
    int id;
    double balance;
    @Deprecated
    public Account(){
    };
    public Account(int id){       this.id = id;    };
    public void transact(double amount){ balance = balance+amount; }
}

class NewAccount extends Account{
    public NewAccount(){ }
}
class TestClass {
    public static void main(String[] args) {
        Account c = new NewAccount();
    }
}*/
class m{
    @SuppressWarnings("unchecked")
    static void doElements(List l) {
        l.add("string"); //1
        System.out.println(l.get(0)); //2
    }
}