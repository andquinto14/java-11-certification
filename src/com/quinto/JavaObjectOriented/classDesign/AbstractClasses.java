package com.quinto.JavaObjectOriented.classDesign;

// REGLAS CLASES ABSTRACTAS

// 1. Las clases hijas deben implementar los metodos abstractor
// 2. Si hay al menos un metodo abstracto la clase debe ser abstracta
// 3. La clase puede ser abstracta sin tener metodos abstractos
// 4. Las clases abstractas no se pueden instanciar
// 5. Una clase no puede ser abstracta y final al mismo tiempo
// 6. No se puede tener un metodo abstracto y privado al mismo tiempo
// 7. No se puede tener un metodo abstracto y static al mismo tiempo
// 8. Una clase abstracta puede extender de otra abstracta
/////////////////////////////////////////////////////////////////////////////
// 1. Las clases abstractas no pueden ser instanciadas
// 2. Todos los timpos de nivel top, incluyendo clases abstractas, no puden ser protected o private
// 3. Las clases abstractas no pueden ser fina
// 4. Pueden tener 0 o mas metodos abstractos y no abstractos
// 5. Una clase abstracta que hereda de otra clase abstracta, hereda todos sus metodos abstractos
// 6. La primera clase concreta que herede de una clase abstracta, debe proveer una implementacion
//    para todos los metodos abstractos heredados
// 7. Los constructores de las clases abstractas siguen las mismas reglas para inicializar constructores
//    regulares, excepto que ellas solo pueden ser llamadas como parte de la inicializacion de una subclase.

// REGLAS METODOS ABSTRACTOS

// 1. Solo pueden ser definidos en clases abstractas o interfaces
// 2. No pueden ser declarados private o final
// 3. No deben proveer un cuerpo de metodo/implementacion en la clase abstracta donde ellos son declarados
// 4. Implementar un metodo abstracto es una subclase sigue las mismas reglas para sobreescritura de metodo,
//    incluyendo tipos de retorno covariante, declaraciones de excepciones, etc.
// 5. Un metodo abstracto no pued estar en una clase concreta

abstract class Bird {
    public abstract String getName();
    public void printName(){
        System.out.println(getName());
    }
    final void f(){}
}

class Stork extends Bird{

    @Override
    public String getName() {
        return "Stork";
    }

    public static void main(String[] args) {
        new Stork().printName();
    }
}

// CLASES CONCRETAS
abstract class Mammal{
    abstract void showHorn();
    abstract void eatLeaf();
}

abstract class Rhino extends Mammal{
    void showHorn(){

    }
}

class BlackRhino extends Rhino{
    @Override
    void eatLeaf() {

    }
}