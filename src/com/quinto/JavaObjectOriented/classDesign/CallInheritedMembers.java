package com.quinto.JavaObjectOriented.classDesign;

class Fish {
    protected int size;
    private int age;

    static {
        System.out.println("static");
    }

    public Fish(int age){
        this.age = age;
    }
    public int getAge(){
        return age;
    }
}

class Shark extends Fish{
    private int numberofFins = 8;
    public Shark(int age){
        super(age);
        this.size = 5;
    }
    public void displaySharkDetails(){
        System.out.println("Age: " + getAge());
        System.out.println("Size: " + size);
        System.out.println("Number of fins: " + numberofFins);
    }

    public static void main(String[] args) {
        Shark shark = new Shark(2);
        shark.displaySharkDetails();
    }
}
