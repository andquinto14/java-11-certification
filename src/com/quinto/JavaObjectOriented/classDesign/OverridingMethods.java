package com.quinto.JavaObjectOriented.classDesign;

// REGLAS PARA SOBREESCRITURA DE METODOS
// 1. El metodo en la clase hija debe tener la misma firma que el metodo en la clase padre.

// 2. El metodo de la clase hija debe ser al menos tan accesible que el metodo de la clase padre.

// 3. El metodo de la clase hija no puede declarar una excepcion checkeada que es nueva o de más ambito
//    que la clase excepcion declarada en el metodo de la clase padre.

// 4. Si el método retorna un valor, este debe ser el mismo o un suptipo del tipo de retorno del metodo en
//    la clase padre, conocido tambien como tipos de retorno covariante.


class Canine {

    public double getAverageWeight(){
        return 50;
    }
}

class Wolf extends Canine{
    @Override
    public double getAverageWeight(){
        // return getAverageWeight() + 20; StackOverflowError
        return super.getAverageWeight() + 20;
    }

    public static void main(String[] args) {
        System.out.println("Canine: " + new Canine().getAverageWeight());
        System.out.println("Wolf: " + new Wolf().getAverageWeight());
    }
}
