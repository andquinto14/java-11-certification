package com.quinto.JavaObjectOriented.classDesign;

// Por defecto el hijo hereda los metodos publicos y protected
// Private solamente si estan en el mismo paquete

// REGLAS DE LOS CONSTRUCTORES
// 1. La primera sentencia de cada constructor es una llamada a un constructor sobrecargado via this()
//    o a un constructor padre usando super().

// 2. Si la primera sentencia de un constructor no es una llamada a this() o super(), el compilador
//    insertará una llamada super() sin argumentos como la primera sentencia del constructor.

// 3. Llamar a this() y super() despues de la primera sentencia en un constructor, nos da error de compila.

// 4. Si la clase padre no tiene un construtor sin argumentos, entonces cada constructor en la clase hija
//    debe iniciar con un explicito this() o super().

// 5. Si la clase padre no tiene un constructor si nargumentos y la clase hija no ha definido argumentos
//    entonces, la clase hija no va a compilar.

// 6. Si la clase solo define constructores privados, entonces esta no puede ser heredada por una clase top.

// 7. Todas las variables de instancia final deben tener un valor asignado solo una vez antes de que termine
//    o finalice el constructor. Cualquier variable de instancia final que no tiene valor asignado será
//    reportado por el compilador como un error en la linea donde el constructor es declarado.

class GiraffeFamily {
    static {
        System.out.print("A");
    }
    {
        System.out.print("B");
    }
    public GiraffeFamily(String name){
        this(1);
        System.out.print("C");
    }
    public  GiraffeFamily(){
        System.out.print("D");
    }
    public GiraffeFamily(int stripes){
        System.out.print("E");
    }
}

class Okapi extends GiraffeFamily{
    static {
        System.out.print("P");
    }
    public Okapi(int stripes){
        super("sugar");
        System.out.print("G");
    }
    {
        System.out.print("H");
    }

    public static void main(String[] args) {
        new Okapi(1);
        System.out.println();
        new Okapi(2);
    }
}
