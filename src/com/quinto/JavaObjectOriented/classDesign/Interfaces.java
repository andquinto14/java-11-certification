package com.quinto.JavaObjectOriented.classDesign;

// REGLAS PARA DECLARAR INTERFACES

// 1. Un archivo Java puede tener solo una clase publica de nivel top o interface, y esta debe tener el
//    mismo nombre que el archivo Java
// 2. Una clase de nivel top o interface, solo puede ser declarada con nivel de acceso public p package-private.
// 3. Las interfaces son abstractas
// 4. Interface variables are assumed to be public, static and final
// 5. Interface methods without a body are assumed to be abstract and public
// 6. La clase que implemente los metodos de la interfaz debe marcar los metodos como publicos explicitamente
//    sino no compila.
// 7. Los metodos en una clase abstracta por defecto son package-private, en una interfaz son public abstract

///////////////////////////////////////////////////////////////////////////////////////////////////
// 1. An interface can extend another interface
// 2. A class can implement an interface
// 3. A class can extend another class whose ancestor implements an interface
// 4. An interface cannot extend a class
// 5. Interface variables are assumed to be public, static and final
// 6. Because interface variables are market final, they must be initialized.


import java.util.List;

abstract interface Robot {
    void on();
    void off();
    static final double PI = 3.14159;

    public default void test(){

    }

    static void eat(){

    }
}

interface Nocturnal {
    int hunt();
}

interface HasBigEyes extends Nocturnal, CanFly{

}

interface CanFly extends Nocturnal{
    void flap();
}

class Owl implements Nocturnal, CanFly{

    @Override
    public int hunt() {
        return 5;
    }

    @Override
    public void flap() {
        System.out.println("Flap");
    }
}

// MODIFICADORES IMPLICITOS

interface Soar{
    int MAX_HEIGHT = 10; // -> public final static int MAX_HEIGHT = 10;
    boolean UNDERWATER = true; // -> public final static boolean UNDERWATER = true;
    void fly(int speed); // -> public abstract void fly(int speed);
}

interface Dance{
    // private int count = 4; NO COMPILE
    // protected void step(); NO COMPILE
}

//private final interface Crawl{ NO COMPILE
    // String distance; NO COMPILE
    // private int MAX = 10; NO COMPILE
    // protected abstract boolean UNDER = false; NO COMPILE
    // private void dig(int depth); NO COMPILE
    // protected abstract double depth(); NO COMPILE
    // public final void surface(); NO COMPILE
//}

// HERENCIA DE INTERFACES

interface HasTail1{
    public int getTailL();
}

interface HasWhiskers{
    public int getNumber();
}

abstract class HarborSeal implements HasTail1, HasWhiskers{

}

//class CommonSeal extends HarborSeal{
    // NO COMPILE, have to implement all methods
//}

// METODOS DUPLICADOS EN INTERFACES

interface Herbivore{
    public void eatPlants();
}

interface Omnivore{
    public void eatPlants();
    public void eatMeat();
}

class Bear implements Herbivore, Omnivore{

    @Override
    public void eatPlants() { // The compiler override both abstract methods

    }

    @Override
    public void eatMeat() {

    }
}

// METODOS DUPLICADOS CON LA MISMA FIRMA

interface Dances{
    String swingArms();
}

interface EatFish{
    CharSequence swingArms();
}

class Penguin implements Dances, EatFish{
    @Override
    public String swingArms() {
        return "swing";
    }

    public static void main(String[] args) {
        System.out.println(new Penguin().swingArms());
    }
}

// INTERFACES Y EL OPERADOR instanceof
/*
class Tet{

    public static void main(String[] args) {
        Number tickets = 4;
        if(tickets instanceof String){}

        if(tickets instanceof List){}

        Integer ticks = 6;
        if(ticks instanceof List){}
    }
}

class MyNumber extends Number implements List{

}*/

interface I { }

class A implements I{
    public String toString(){ return "in a"; }
}

class B extends A{
    public String toString(){ return "in b"; }
}


class TestClass {

    public static void main(String[] args) {
        B b = new B();
        A a = b;
        I i = a;

        System.out.println(i);
        System.out.println((B)a);
        System.out.println(b);

    }
}