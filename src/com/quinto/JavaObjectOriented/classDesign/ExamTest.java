package com.quinto.JavaObjectOriented.classDesign;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

// REGLAS DE LAS CLASES

// 1. Classes declared as members of top-level classes can be declared static.
// 2. Anonymous classes cannot be declared static.
// 3. The modifier static pertains only to member classes, not to top level or local or anonymous classes.
//    That is, only classes declared as members of top-level classes can be declared static.
//    Package member classes, local classes (i.e. classes declared in methods) and anonymous classes
//    cannot be declared static.
// 4. When a class, whose members should be accessible only to members of that class,
//    is coded such a way that its members are accessible to other classes as well,
//    this is called weak encapsulation.
// 5. If you make an anonymous class from another class then it extends from that class,
// 6. A static nested class can contain a non - static inner class.
// 7. Multiple inheritance of state includes ability to inherit instance fields from multiple classes.
// 8. Multiple inheritance of type includes ability to implement multiple interfaces and/or ability to
//    extend from multiple classes.


class A1 {
    public int getCode() {
        return 2;
    }
}

class AA extends A1 {
    public void doStuff() {
    }
}

/*class m{
    public static void main(String[] args) {
        A1 a = null;
        AA aa = null;

        //a = (AA) aa;
        //a = new AA();
        //a = (AA)a;
        aa = a;
        ((AA)a).doStuff();

    }
}*/
/*
class Bird {
    private Bird(){     }
}
class Eagle extends Bird {
    public String name;
    public Eagle(String name){
        this.name = name;
    }

    public static void main(String[] args) {
        System.out.println(new Eagle("Bald Eagle").name);
    }
}*/
/*
class Super { static String ID = "QBANK"; }

class Sub extends Super{
    static { System.out.print("In Sub"); }
}
class Test{
    public static void main(String[] args){
        System.out.println(Sub.ID);
    }
}*/
/*
class Data {

    int intVal = 0;
    String strVal = "default";
    public Data(int k){
        this.intVal = k;
    }

}

class TestClass1 {
    public static void main(String[] args) throws Exception {
        Data d1 = new Data(10);
        d1.strVal = "D1";
        Data d2 = d1;
        d2.intVal = 20;
        System.out.println("d2 val = "+d2.strVal);
    }
}*/
/*
interface House{
    public default String getAddress(){
        return "101 Main Str";
    }
}

interface Office {
    public default String getAddress(){
        return "101 Smart Str";
    }
}

class HomeOffice implements House, Office{
    public String getAddress(){
        Double a;
        return "R No 1, Home";
    }
}

class TestClass1 {

    public static void main(String[] args) {
        House h = new HomeOffice();  //1
        System.out.println(h.getAddress()); //2
    }
}*/
/*
class TestClass1{
    void probe(Object x) { System.out.println("In Object"); } //3

    void probe(Number x) { System.out.println("In Number"); } //2

    void probe(Integer x) { System.out.println("In Integer"); } //2

    void probe(Long x) { System.out.println("In Long"); } //4

    public static void main(String[] args){
        double a = 10;
        new TestClass1().probe(a);
    }
}*/
/*
class InitTest{
    static int si = 10;
    int  i;
    final boolean bool;
    // 1
    { bool = (si > 5); i = 1000; }

}*/
/*
interface Tool{    //1
    void operate(); //2
}

abstract class PowerTool implements Tool{ } //3

class PowerSaw implements PowerTool{  //4
    @Override //5
    public void operate(){ }
}

class SteamPowerSaw extends PowerSaw{ //6
    @Override //7
    public void operate(int pressure){ }
}*/
/*
class Super{
    public int getNumber( int a){
        return 2;
    }
}
class SubClass extends Super{
    public int getNumber( int a, char ch){
        return 4;
    }
    public static void main(String[] args){
        System.out.println( new SubClass().getNumber(4) );
    }
}*/
/*
class Base{
    public <T extends CharSequence> Collection<String> transform(Collection<T> list)
    {
        return new ArrayList<String>();
    }

}*/
/*
interface Movable {
    int location = 0;
    void move(int by);
    public void moveBack(int by);
}

class Donkey implements Movable{
    int location = 200;
    public void move(int by) {
        location = location+by;
    }
    public void moveBack(int by) {
        location = location-by;
    }
}

class TestClass3 {
    public static void main(String[] args) {
        Movable m = new Donkey();
        m.move(10);
        m.moveBack(20);
        System.out.println(m.location);
    }
}*/
/*class A2{
    public List<Number> getList(){
        //valid code
        return null;
    };
}
class B extends A2{
    @Override
    public List<Object> getList(){
        return null;
    }
        //*INSERT CODE HERE*
    //valid code
};
}*/
/*
class Base{
    void methodA(){
        System.out.println("base - MethodA");
    }
}

class Sub extends Base{
    public void methodA(){
        System.out.println("sub - MethodA");
    }
    public void methodB(){
        System.out.println("sub - MethodB");
    }
    public static void main(String args[]){
        Base b=new Sub(); //1
        b.methodA(); //2
        b.methodB(); //3
    }
}*/
/*interface Classic {
    int version = 1;
    public void read() ;
}

class MediaReader implements Classic{
    int version = 2;
    public void read() {
        //Insert code here
        System.out.println(((Classic)this).version);
        System.out.println(version);
    }
}

class ReaderTest{
    public static void main(String[] args) {
        MediaReader mr = new MediaReader();
        mr.read();
    }
}*/
/*
class Onion {
    private String data = "skin";

    private class Layer extends Onion {
        String data = "thegoodpart";

        public String getData() {
            return data;
        }
    }

    public String getData() {
        return new Layer().getData();
    }

    public static void main(String[] args) {
        var o = new Onion();
        System.out.println(o.getData());
        char ch = 10;
    }
}

class prueba {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 0, -2, 0, 5, -3, 8, -1, 0);
        Stream.of(
                new AbstractMap.SimpleEntry<>("P", numbers.stream().filter(number -> number > 0).count()),
                new AbstractMap.SimpleEntry<>("C", numbers.stream().filter(number -> number == 0).count()),
                new AbstractMap.SimpleEntry<>("N", numbers.stream().filter(number -> number < 0).count())
        ).forEach(entry -> {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        });
    }
}*/
/*
class TestClass{
    long l1;
    public void TestClass(long pLong) { l1 = pLong ; }  //(1)
    public static void main(String args[]){
        TestClass a, b;
        a = new TestClass();  //(2)
        b = new TestClass(5);  //(3)
    }
}*/
/*
class Outsider
{
    public class Insider{ }
}
class TestClas
{
    public static void main(String[] args)
    {
        var os = new Outsider();
        // 1 insert line here
        Outsider.Insider in = os.new Insider();
    }
}*/
/*
interface AmazingInterface{
    String value = "amazing";
    void amazingMethod(String arg);
}

abstract class AmazingClass implements AmazingInterface{
    static String value = "awesome";
    abstract void amazingMethod(String arg1, String arg2);
}


class Awesome extends AmazingClass implements AmazingInterface {
    public void amazingMethod(String arg1){ }
    public void amazingMethod(String arg1, String arg2){ }

    public  void main(String[] args){
        AmazingInterface ai = new Awesome();
        ((AmazingClass)ai).amazingMethod("x1", value);
        ai.amazingMethod(value);
        ai.amazingMethod("x1");
        ai.amazingMethod(AmazingInterface.value, AmazingClass.value);
        ai.amazingMethod(AmazingInterface.value);

        //INSERT CODE HERE
    }

}*/
/*
interface ConstTest{
    public int A = 1; //1
    int B = 1;          //2
    static int C = 1;  //3
    final int D = 1; 	 //4
    public static int E = 1; //5
    public final int F = 1;  //6
    static final int G = 1;    //7
    public static final int H = 1; //8
}*/
/*
class SuperClass {

    public SuperClass(int m) {

    }
}

class SubClass extends SuperClass{
    int i, j, k;
    public SubClass( int m, int n )     {
        super();
        i = m ;  j = m ;  } //1
    public SubClass( int m )  {   super(m );   } //2
}*/
/*class Automobile{
    final String getVin(){ return "1234"; }
}

final class Car extends Automobile{
}*/
/*class A12{
    protected int i;
    A12(int i) {    this.i = i;    }

}

class Bbb { Bbb() {} }

class Be extends A {  }*/
/*class TestClass3
{
    public class A
    {
    }
    public static class B
    {
    }
    public void useClasses()
    {
        //1
        new TestClass.B();

        new A();
    }
}*/
/*
class Baap {
    public int h = 4;
    public int getH() {
        System.out.println("Baap " + h);
        return h;
    }
}

class Beta extends Baap {
    public int h = 44;
    public int getH() {
        System.out.println("Beta " + h);
        return h;
    }
    public static void main(String[] args) {
        Baap b = new Beta();
        System.out.println(b.h + " " + b.getH());
        Beta bb = (Beta) b;
        System.out.println(bb.h + " " + bb.getH());
    }
}

interface IHello2 {
    private static void print(){ };
}

abstract class Hello {
    public short hello(int a, int b){ return 0; } }*/

/*
class BookStore
{
    private static final int taxId = 300000;
    private String name;
    public String searchBook( final String criteria )
    {
        int count = 0;
        int sum = 0;
        sum++;
        class Enumerator
        {
            String iterate( int k)
            {
                //line 1
                System.out.println(taxId);
                System.out.println(name);
                System.out.println(criteria);
                System.out.println(count);
                System.out.println(k);
                System.out.println(sum);
                return "";
            }
            // lots of code.....
        }
        // lots of code.....
        return "";
    }
}*/
/*class TestClassaa{
    void probe(int... x) { System.out.println("In ..."); }  //1

    void probe(Integer x) { System.out.println("In Integer"); } //2

    void probe(long x) { System.out.println("In long"); } //3

    void probe(Long x) { System.out.println("In LONG"); } //4

    public static void main(String[] args){
        Integer a = 4; new TestClassaa().probe(a); //5
        int b = 4; new TestClassaa().probe(b); //6
    }
}*/
/*
interface Drink{
    default double getAlcoholPercent(){
        return 0.0;
    }

    static double computeAlcoholPercent(){
        return 0.0;
    }
}


interface ColdDrink extends Drink{
    String getName();
}

abstract class CrazyDrink implements ColdDrink{

    // INSERT CODE HERE
}*/
/*
interface II{
}

class AAA implements II{
}

class BBB extends AAA {
}

class C extends BBB{
}

class M {
    public static void main(String[] args) {
        AAA a = new AAA();
        B b = new B();
        a = (B)(I)b;
        b = (B)(I) a;
        a = (I) b;
        I i = (C) a;



    }
}*/
