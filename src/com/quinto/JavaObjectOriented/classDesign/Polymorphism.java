package com.quinto.JavaObjectOriented.classDesign;

class Primate {
    public boolean hasHair(){
        return true;
    }
}

interface HasTail{
    public abstract boolean isTailStripped();
}

class Lemur extends Primate implements HasTail{

    public int age = 10;

    @Override
    public boolean isTailStripped() {
        return false;
    }

    public static void main(String[] args) {
        // 1. Usando variable de referencia del mismo tipo
        Lemur lemur = new Lemur();
        System.out.println(lemur.age); // output: 10

        // 2. Interface = Clase que implementa la interface
        HasTail hasTail = lemur;
        System.out.println(hasTail.isTailStripped()); // output: false

        // 3. Clase padre = Clase hija
        Primate primate = lemur;
        System.out.println(primate.hasHair()); // output: true
    }

}