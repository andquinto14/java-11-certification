package com.quinto.JavaObjectOriented;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

class A{
    public void m1() {   }
}
class B extends A{
    public void m1() {   }
}
class C extends B{
    public void m1(){
      /*  //1
      ... lot of code.
      */
        ( (A) this ).m1();
    }
}

class InitClass{
    public static void main(String args[ ] ){
        InitClass obj = new InitClass(5);
    }
    final static private double PI = 3.14159265358979323846 ;

    int m;
    static int i1 = 5;
    static int i2 ;
    int  j = 100;
    int x;
    public InitClass(int m){
        System.out.println(i1 + "  " + i2 + "   " + x + "  " + j + "  " + m);
    }
    { j = 30; i2 = 40; }  // Instance Initializer
    static { i1++; }      // Static Initializer
}

interface House{
    public default void lockTheGates(){
        System.out.println("Locking House");
    }
}

interface Office {
    public void lockTheGates();
}

class HomeOffice implements House, Office{ //1
    public void lockTheGates(){
        System.out.println("Locking HomeOffice");
    }
}

class TestClass {

    public static void main(String[] args) {
        Office off = new HomeOffice();  //2
        off.lockTheGates();
    }
}

/*class AA{
    public List<? extends Number> getList(){
        //valid code
    }
}
class BB extends AA{
    @Override
    public ArrayList<? extends Integer> getList(){}
        //valid code
}*/
class ValueHolder{
    final int value;

    public ValueHolder(int x) { value = x; }

}

class TestClassbb implements I1, I2{
    public void m1() { System.out.println("Hello"); }
    public static void main(String[] args){
        TestClass tc = new TestClass();
        ( (I1) tc).m1();
    }
}
interface I1{
    int VALUE = 1;
    void m1();
}
interface I2{
    int VALUE = 2;
    void m1();
}

class Aa{
    protected int i;
    Aa(int i) {    this.i = i;    }

}

class Bb extends Aa {
    Bb(int i) {
        super(i);
    }
}

class Outer
{
    private double d = 10.0;
    //put inner class here.
    class Inner{
        void m1(){
            d = 20.0;
        }
    }
}

class Test{
    static int a = 0;
    int b = 5;

    public void foo(){
        while(b>0){
            b--;
            a++;
        }
        System.out.println(a);
    }

    public static void main(String[] args) {
        var p1 = new Test();
        p1.foo();
        var p2 = new Test();
        p2.foo();

        System.out.println(p1.a+" "+p2.a);
    }
}

enum Title
{
    MR("Mr."), MS1("Ms."), MS2("Ms.");
    private String title;
    private Title(String s){
        title = s;
    }
}

class TestClassse{
    public static void main(String[] args) {
        var ts = new TreeSet<Title>();
        ts.add(Title.MS2);
        ts.add(Title.MR);
        ts.add(Title.MS1);
        for(Title t :  ts){
            System.out.println(t);
        }
    }
}

interface Flyer{ String getName(); }

class Bird implements Flyer{
    public String name;
    public Bird(String name){
        this.name = name;
    }
    public String getName(){ return name; }
}

class Eagle extends Bird {
    public Eagle(String name){
        super(name);
    }
}

class TestClasvs {
    public static void main(String[] args) throws Exception {
        Flyer f = new Eagle("American Bald Eagle");
        //PRINT NAME HERE
        System.out.println(((Eagle) f).getName());
    }
}

enum Coffee
{
    ESPRESSO("Very Strong"), MOCHA("Bold"), LATTE("Mild");
    public String strength;
    Coffee(String strength)
    {
        this.strength = strength;
    }
    public String toString(){  return String.valueOf(strength);}



}

class As{
    public double m1(int a){
        return a*10/4-30;
    }
}
class A2s extends As{
    public double m1(int a){
        return a*10/4.0;
    }
}

class TestClascs{
    private int x;
    public static void main(String[] args){
        // lot of code.

    }
}