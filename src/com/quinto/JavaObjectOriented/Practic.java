package com.quinto.JavaObjectOriented;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/*class TestClass implements T1, T2{
    public void m1(){
        System.out.println("Hola");
    }
}
interface T1{
    int VALUE = 1;
    void m1();
}
interface T2{
    int VALUE = 2;
    void m1();
}
public class Practic {
    public static void main(String[] args) {
        TestClass testClass = new TestClass();
        testClass.m1();
        TestClass a, b;
    }
}*/
// Filename: TestClass.java
/*
class TestClass{
    public static void main(String args[]){
        B c = new C();
        System.out.println(c.max(10, 20));
    }
}
class A{
    int max(int x, int y)  { if (x>y) return x; else return y; }
}
class B extends A{
    int max(int x, int y)  {  return 2 * super.max(x, y) ; }
}
class C extends B{
    int max(int x, int y)  {  return super.max( 2*x, 2*y); }
}*/
/*
interface Bozo{
    int type = 0;
    public void jump();
}


public class Type1Bozo implements Bozo{
    public Type1Bozo(){
        type = 1;
    }

    public void jump(){
        System.out.println("jumping..."+type);
    }

    public static void main(String[] args){
        Bozo b = new Type1Bozo();
        b.jump();
    }
}
*/
/*
class Super{
    public String toString(){
        return "4";
    }
}
class SubClass extends Super{
    public String toString(){
        return super.toString()+"3";
    }
    public static void main(String[] args){
        System.out.println( new SubClass() );
    }
}*/
/*
class A {
}

class AA extends A {
}


class TestClass {
    public static void main(String[] args) throws Exception {
        A a = new A();
        AA aa = new AA();
        a = aa;
        System.out.println("a = "+a.getClass());
        System.out.println("aa = "+aa.getClass());
    }
}*/
/*class Sample implements IInt{
    public static void main(String[] args){
        Sample s = new Sample();  //1
        int j = s.thevalue;       //2
        int k = IInt.thevalue;    //3
        int l = thevalue;         //4
    }
}
interface IInt{
    int thevalue = 0;
}*/
/*class StaticTest{

    void m1(){
        StaticTest.m2();  // 1
        m4();             // 2
        StaticTest.m3();  // 3
    }

    static void m2(){ }  // 4

    void m3(){
        m1();            // 5
        m2();            // 6
        StaticTest.m1(); // 7
    }

    static void m4(){ }
}*/
/*class Triangle{
    public int base;
    public int height;
    private final double ANGLE;

    public  void setAngle(double a){  ANGLE = a;  }

    public static void main(String[] args) {
        var t = new Triangle();
        t.setAngle(90);
    }
}*/
/*class InitTest{
    public InitTest(){
        s1 = sM1("1");
    }
    static String s1 = sM1("a");
    String s3 = sM1("2");{
        s1 = sM1("3");
    }
    static{
        s1 = sM1("b");
    }
    static String s2 = sM1("c");
    String s4 = sM1("4");
    public static void main(String args[]){
        InitTest it = new InitTest();
    }
    private static String sM1(String s){
        System.out.println(s);  return s;
    }
}*/
/*class Outer
{
    private void Outer() { }
    protected class Inner
    {
    }
}*/
/*
enum Coffee
{
    ESPRESSO("Very Strong"), MOCHA("Bold"), LATTE("Mild");
    public String strength;
    Coffee(String strength)
    {
        this.strength = strength;
    }
    public String toString(){   return strength; }
}
class m{
    public static void main(String[] args) {
        List.of(Coffee.values()).stream().forEach(
                e->{System.out.print(e.name()+":"+e+", ");});

    }
}
*/
/*
interface Eatable{
    int types = 10;
}
class Food implements Eatable {
    public static int types = 20;
}
class Fruit extends Food implements Eatable{  //LINE1

    public static void main(String[] args) {
        Food.types = 30; //LINE 2
        System.out.println(Food.types); //LINE 3
    }
}*/
/*
class A {
    public int getCode(){ return 2;}
}

class AA extends A {
    public long getCode(){ return 3;}
}

public class TestClass {

    public static void main(String[] args) throws Exception {
        A a = new A();
        A aa = new AA();
        System.out.println(a.getCode()+" "+aa.getCode());
    }

    public int getCode() {
        return 1;
    }
}*/
/*class Sample implements IInt{
    public static void main(String[] args){
        Sample s = new Sample();  //1
        int j = s.thevalue;       //2
        int k = IInt.thevalue;    //3
        int l = thevalue;         //4
    }
}
interface IInt{
    int thevalue = 0;
}*/
/*
class A{
    public List<Number> getList(){
        //valid code
    };
}
class B extends A{
    @Override
    public ArrayList<Object> getList(){

    }
        //valid code
};
*/
/*
interface House{
    public default String getAddress(){
        return "101 Main Str";
    }
}

interface Office {
    public default String getAddress(){
        return "101 Smart Str";
    }
}

class HomeOffice implements House, Office{
    public String getAddress(){
        return "R No 1, Home";
    }
}

class TestClass {

    public static void main(String[] args) {
        House h = new HomeOffice();  //1
        System.out.println(h.getAddress()); //2
        char ch = 10;
        Thread t = new Runnable() {
            @Override
            public void run() {

            }
        };
        Runnable r = new Thread();
        Object o = new java.io.File("a.txt");
    }
}*/
/*
class TestOuter
{
    public void myOuterMethod()
    {
        // 1
        new TestInner();
    }
    public class TestInner { }
    public static void main(String[] args)
    {
        var to = new TestOuter();
        // 2
        new TestOuter.TestInner();
        new TestOuter().new TestInner();

    }
}*/
/*
interface T1{
}
interface T2{
    int VALUE = 10;
    void m1();
}

interface T3 extends T1, T2{
    public void m1();
    public void m1(int x);
}*/
/*
class ParamTest {

    public static void printSum(double a, double b){
        System.out.println("In double "+(a+b));
    }
    public static void printSum(float a, float b){
        System.out.println("In float "+(a+b));
    }

    public static void main(String[] args) {
        printSum(1.0, 2.0);
    }
}*/

/*
interface House{
    public default void lockTheGates(){
        System.out.println("Locking House");
    }
}

interface Office {
    public void lockTheGates();
}

class HomeOffice implements House, Office{ //1
    public void lockTheGates(){
        System.out.println("Locking HomeOffice");
    }
}

class TestClass {

    public static void main(String[] args) {
        Office off = new HomeOffice();  //2
        off.lockTheGates();
    }
}*/
/*
interface ConstTest{
    public int A = 1; //1
    int B = 1;          //2
    static int C = 1;  //3
    final int D = 1; 	 //4
    public static int E = 1; //5
    public final int F = 1;  //6
    static final int G = 1;    //7
    public static final int H = 1; //8
}*/
/*
public class TopClass
{
    public Inner inner1 = new Inner()
    {
        public void doA(){  System.out.println("doing A"); }
    };

    public void doA() { inner1.doA(); }
}

class Inner
{
    int value;
}*/
/*
class Base{
    public short getValue(){ return 1; } //1
}
class Base2 extends Base{
    public byte getValue(){ return 2; } //2
}
class TestClass{
    public static void main(String[] args){
        Base b = new Base2();
        System.out.println(b.getValue()); //3
    }
}*/
/*
class TestClass
{
    public class A{
    }
    public static class B {
    }
    public static void main(String args[]){
        class C{
        }
        //1
        new TestClass().new A();
        new TestClass().new B();
        new C();
        new TestClass().C();
    }
}*/
/*
abstract class Bang{
    // abstract void f();  // LINE 0
    final    void g(){}
    // final    void h(){} // LINE 1
    protected static int i;
    private int j;
}

final class BigBang extends Bang{
    // BigBang(int n) { m = n; } // LINE 2
    public static void main(String args[]){
        Bang mc = new BigBang();
    }
    // @Override // LINE 3
    void h(){}
    void k(){ i++; } // LINE 4
    void l(){ j++; } // LINE 5
    int m;
}*/
/*
interface Eatable{
    int types = 10;
}
class Food implements Eatable {
    public static int types = 20;
}
class Fruit extends Food implements Eatable{  //LINE1

    public static void main(String[] args) {
        types = 30; //LINE 2
        System.out.println(types); //LINE 3
    }
}*/
/*
class StaticTest{

    void m1(){
        StaticTest.m2();  // 1
        m4();             // 2
        StaticTest.m3();  // 3
    }

    static void m2(){ }  // 4

    void m3(){
        m1();            // 5
        m2();            // 6
        StaticTest.m1(); // 7
    }

    static void m4(){ }
}*/
/*
interface Flyer{ String getName(); }

class Bird implements Flyer{
    String name;
    public Bird(String name){
        this.name = name;
    }
    public String getName(){ return name; }
}

class Eagle extends Bird {
    public Eagle(String name){
        super(name);
    }
}

class TestClass {
    public static void main(String[] args) throws Exception {
        Flyer f = new Eagle("American Bald Eagle");
        //PRINT NAME HERE
        System.out.println(f.getName());
        System.out.println(((Eagle)f).name);
        System.out.println(((Bird)f).getName());

    }
}*/
/*
interface Pow{
    static void wow(){
        System.out.println("In Pow.wow");
    }
}

abstract class Wow{

    static void wow(){  // LINE 9
        System.out.println("In Wow.wow");
    }
}

class Powwow extends Wow implements Pow {
    public static void main(String[] args) {
        Powwow f = new Powwow();
        f.wow();
    }
}*/
/*
interface I { }

class A implements I{
    public String toString(){ return "in a"; }
}

class B extends A{
    public String toString(){ return "in b"; }
}


class TestClass {

    public static void main(String[] args) {
        B b = new B();
        A a = b;
        I i = a;

        System.out.println(i);
        System.out.println((B)a);
        System.out.println(b);

    }
}*/
/*
class TestClass{
    public static void main(String[] args){
        new TestClass().sayHello(); }   //1
    public static void sayHello(){
        System.out.println("Static Hello World"); }  //2
    public void sayHello() {
        System.out.println("Hello World "); }  //3
}*/
/*class Automobile{
    public void drive() {  System.out.println("Automobile: drive");   }
    static class g{
        int a = 1;
    }
}*/

/*
class Truck extends Automobile{
    public void drive() {  System.out.println("Truck: drive");   }
    public static void main (String args [ ]){
        Automobile  a = new Automobile();
        Truck t  = new Truck();
        a.drive(); //1
        t.drive(); //2
        a = t;     //3
        a.drive(); //4
    }
}*/
/*
interface Processor {
    Iterable process();
}

interface ItemProcessor extends Processor{
    Collection process();
}

interface WordProcessor extends Processor{
    String process();
}

interface GenericProcessor extends ItemProcessor, WordProcessor{

}*/
/*
final class p{
    class l{

    }
}*/
/*
public class TestClass {

    public static void main(String[] args) throws Exception {
        work();                  //LINE 10
        int j = j1;               //LINE 11
        int j1 = (double) x; //LINE 12
    }

    public static void work() throws Exception{
        System.out.println(x); //LINE 15
    }

    static double x;    //19
}*/
/*
class TestClass{
    void probe(int... x) { System.out.println("In ..."); }  //1

    void probe(Integer x) { System.out.println("In Integer"); } //2

    void probe(long x) { System.out.println("In long"); } //3

    void probe(Long x) { System.out.println("In LONG"); } //4

    public static void main(String[] args){
        Integer a = 4; new TestClass().probe(a); //5
        int b = 4; new TestClass().probe(b); //6
    }
}*/
import java.util.*;
/*
class TestClass{
    public OtherClass oc = new OtherClass();
}
class OtherClass{
    int value;
}*/
class Gorilla{
    public static int count;
    public static void addGorilla(){
        count++;
    }
    public void babyGorilla(){
        count++;
    }
    public void announceBabies(){
        addGorilla();
        babyGorilla();
    }
    public static void announceBabiesToEveryone(){
        addGorilla();
        // babyGorilla(); NO COMPILE
    }
    public int total;
    // public static double average = total/count; // NO COMPILE
}

class Counter{
    private static int count;
    public Counter(){
        count++;
    }

    public static void main(String[] args) {
        Counter c1 = new Counter();
        Counter c2 = new Counter();
        Counter c3 = new Counter();
        System.out.println(count); // print 3
    }
}

class Initializers{
    public static final int NUM_BUCKET = 45;
    private static final ArrayList<String> values = new ArrayList<>();
    // private static final int FOUR; NO COMPILE

    public static void main(String[] args) {
        // NUM_BUCKET = 3; NO COMPILE
        values.add("Andres");
        // values = new ArrayList<>(); NO COMPILE
    }
}

class ReturningValues{
    public static void main(String[] args) {
        int number = 1;
        String letters = "abc";
        number(number);
        letters = letters(letters);
        System.out.println(number + letters);
    }

    private static String letters(String letters) {
        letters += "d";
        return letters;
    }

    private static int number(int number) {
        number++;
        return number;
    }
}

class Overloading{
    public void fly(int numMiles){
        System.out.println("int");
    }
    public void fly(short numFeet){
        System.out.println("short");
    }
    public boolean fly(){return false;}
    void fly(int numMiles, short numFeet){}
    public void fly(short numFeet, int numMiles) throws Exception{}

    // public void fly(int numMiles){} NO COMPILE
    // public int fly(int numMiles){} NO COMPILE
    public static void main(String[] args) {
        Overloading overloading = new Overloading();
        overloading.fly((short) 1);
    }
}

class OverloadingReferenceTypes{
    public void fly(String s){
        System.out.print("string");
    }
    public void fly(Object o){
        System.out.print ("object");
    }

    public static void main(String[] args) {
        OverloadingReferenceTypes referenceTypes = new OverloadingReferenceTypes();
        referenceTypes.fly("test");
        System.out.print("-");
        referenceTypes.fly(56);
    }
}

class OverloadingGenerics{
    // public void walk(List<String> strings){} NO COMPILE
    // public void walk(List<Integer> integers){} NO COMPILE
    // public void walk(List strings){} NO COMPILE
    // public void walk(List integers){} NO COMPILE
}

class OverloadingArrays{
    public static String glide(String s){
        return "1";
    }
    public static String glide(String... s){
        return "2";
    }
    public static String glide(Object o){
        return "3";
    }
    public static String glide(String s, String t){
        return "4";
    }

    public static void play(Long l){}
    public static void play(Long... l){}

    public static void main(String[] args) {
        System.out.println(glide("a"));
        System.out.println(glide("a", "b"));
        System.out.println(glide("a", "b", "c"));
        // play(4); NO COMPILE
        play(4L);
    }
}