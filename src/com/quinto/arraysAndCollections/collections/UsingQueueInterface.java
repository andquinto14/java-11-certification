package com.quinto.arraysAndCollections.collections;

// IMPLEMENTATIONS
// 1. LinkedList -> Implements list and Queue, good to use with Queue
// 2. ArrayDeque ->

import java.util.LinkedList;
import java.util.Queue;

public class UsingQueueInterface {
    // FACTORY METHODS
    public static void main(String[] args) {
        Queue<Integer> queue = new LinkedList<>();
        System.out.println(queue.offer(10)); // true
        System.out.println(queue.offer(4)); // true
        System.out.println(queue.peek()); // 10
        System.out.println(queue.poll()); // 10
        System.out.println(queue.poll()); // 4
        System.out.println(queue.peek()); // null

        // Can trow an exception
        /*queue.add(2); //
        queue.element();
        queue.remove();*/
    }
}
