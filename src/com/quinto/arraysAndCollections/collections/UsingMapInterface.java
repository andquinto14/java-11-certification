package com.quinto.arraysAndCollections.collections;

// IMPLEMENTATIONS
// 1. HashMap ->
// 3. TreeMap ->

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.BiFunction;

public class UsingMapInterface {
    // FACTORY METHODS
    public static void main(String[] args) {
        // Immutable maps
        Map.of("key1", "value1", "key2", "value2");
        // Map.of("key1", "value1", "key2"); INCORRECT
        Map<String, String> map = Map.ofEntries(
                Map.entry("key1", "value1"),
                Map.entry("key2", "value2")
        );
        Map.copyOf(map);

        // Methods

        // HashMap
        Map<String, String> animals = new HashMap<>();
        animals.put("koala", "bamboo");
        animals.put("lion", "meat");
        animals.put("giraffe", "leaf");
        String food = animals.get("koala"); // bamboo
        for (String key: map.keySet()){
            System.out.println(key + ","); // koala,giraffe,lion, -> unordered
        }

        // TreeMap
        Map<String, String> animals1 = new TreeMap<>();
        animals1.put("koala", "bamboo");
        animals1.put("lion", "meat");
        animals1.put("giraffe", "leaf");
        String food1 = animals1.get("koala"); // bamboo
        for (String key: map.keySet()){
            System.out.println(key + ","); // giraffe,koala,lion, -> -> ordered
        }

        //System.out.println(animals.contains("lion")); // contains method is from Collection

        System.out.println(animals.containsKey("lion")); // true
        System.out.println(animals.containsValue("lion")); // false
        System.out.println(animals.size()); // 3
        animals.clear();
        System.out.println(animals.size()); // 0
        System.out.println(animals.isEmpty()); // true

        // forEach() and entrySet()
        Map<Integer, Character> mapp = new HashMap<>();
        mapp.put(1, 'a');
        mapp.put(2, 'b');
        mapp.put(3, 'c');
        mapp.forEach((k, v) -> System.out.println(v));
        mapp.values().forEach(System.out::println);

        mapp.entrySet().forEach(e -> System.out.println(e.getKey() + ": " + e.getValue()));

        // getOrDefault()
        Map<Character, String> myMap = new HashMap<>();
        myMap.put('x', "spot");
        System.out.println(myMap.getOrDefault("x", "")); // spot
        System.out.println(myMap.getOrDefault("y", "")); // vacio

        // replace() replaceAll()
        Map<Integer, Integer> mapInt = new HashMap<>();
        mapInt.put(1, 2);
        mapInt.put(2, 4);
        Integer original = mapInt.replace(2, 10); // 4
        System.out.println(mapInt); // {1=2, 2=10}

        mapInt.replaceAll((k, v) -> k + v);
        System.out.println(mapInt); // {1=3, 2=12}

        // putIfAbsent()
        Map<String, String> favorites = new HashMap<>();
        favorites.put("Jenny", "Bus Tour");
        favorites.put("Tom", null);
        favorites.putIfAbsent("Jenny", "Tram");
        favorites.putIfAbsent("Sam", "Tram");
        favorites.putIfAbsent("Tom", "Tram");
        System.out.println(favorites); // {Tom=Tram, Jenny=Bus Tour, Sam=Tram}

        // merge()
        BiFunction<String, String, String> mapper = (v1, v2) -> v1.length() > v2.length() ? v1 : v1;
        Map<String, String> favs = new HashMap<>();
        favorites.put("Jenny", "Bus Tour");
        favorites.put("Tom", "Tram");

        String jenny = favs.merge("Jenny", "Skyride", mapper);
        String tom = favs.merge("Tom", "Skyride", mapper);

        System.out.println(favs); // {Tom=Skyride, Jenny=Bus Tour}
        System.out.println(jenny); // Bus Tour
        System.out.println(tom); // Skyride
    }
}