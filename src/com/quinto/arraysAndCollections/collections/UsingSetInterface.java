package com.quinto.arraysAndCollections.collections;

// IMPLEMENTATIONS
// 1. HashSet -> save elements on a hash table, the keys are hash and the values are objects
// 2. TreeSet -> Save elements on a tree ordered structure, object must implement Comparable interface

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class UsingSetInterface {
    // FACTORY METHODS
    public static void main(String[] args) {
        // Immutable Sets
        Set<Character> letters = Set.of('z', 'o', 'y');
        Set<Character> copy = Set.copyOf(letters);

        // HashSet
        Set<Integer> set = new HashSet<>();
        boolean b1 = set.add(66); // true
        boolean b2 = set.add(10); // true
        boolean b3 = set.add(66); // false
        boolean b4 = set.add(8); // true
        set.forEach(System.out::println); // 66, 8, 10 -> imprime de forma aleatoria

        System.out.println("---------------------");

        // TreeSet
        Set<Integer> set2 = new TreeSet<>();
        boolean b11 = set2.add(66); // true
        boolean b22 = set2.add(10); // true
        boolean b33 = set2.add(66); // false
        boolean b44 = set2.add(8); // true
        set2.forEach(System.out::println); // 8, 10, 66 -> imprime de forma ordenada
    }

}

class Ducky implements Comparable<Ducky> {
    private String name;

    public Ducky(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Ducky o) {
        return name.compareTo(o.name); // sorts ascending by name
    }

    public static void main(String[] args) {
        var ducks = new ArrayList<Ducky>();
        ducks.add(new Ducky("Quack"));
        ducks.add(new Ducky("Puddles"));
        Collections.sort(ducks); // sort by name, the parameter should be a collection that implements Comparable
        System.out.println(ducks); // [Puddles, Quack]
    }
}

// Example
class UseTreeSet{
    static class Rabbit{
        int id;
    }

    public static void main(String[] args) {
        Set<Ducky> ducks = new TreeSet<>();
        ducks.add(new Ducky("Lucas"));

        Set<Rabbit> rabbits = new TreeSet<>();
        rabbits.add(new Rabbit()); // java.lang.ClassCastException, Rabbit not implement Comparable

        Set<Rabbit> rabbits2 = new TreeSet<>((r1, r2) -> r1.id - r2.id);
        rabbits2.add(new Rabbit());  // COMPILE
    }
}
