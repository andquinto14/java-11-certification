package com.quinto.arraysAndCollections.collections;

// IMPLEMENTATIONS
// 1. ArrayList -> recommended when many element reading
// 2. LinkedList -> Implements list and Queue, good to use with Queue

// OJO: Methods in list interface works with indices

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class UsingListInterface {
    // FACTORY METHODS

    public static void main(String[] args) {
        // Arrays.asList() -> returns fixed size list backed by an array
        String[] array = new String[]{"a", "b", "c"};
        List<String> asList = Arrays.asList(array);

        // List.of() -> returns immutable list
        List<String> of = List.of(array);

        // List.copyOf() -> returns immutable list
        List<String> copy = List.copyOf(asList);

        System.out.println(asList); // ["a", "b", "c"]
        System.out.println(of); // ["a", "b", "c"]
        System.out.println(copy); // ["a", "b", "c"]

        array[0] = "z";

        System.out.println("-----------------------------");
        System.out.println(asList); // ["z", "b", "c"]
        System.out.println(of); // ["a", "b", "c"]
        System.out.println(copy); // ["a", "b", "c"]

        asList.set(0, "x");

        System.out.println("-----------------------------");
        System.out.println(asList); // ["x", "b", "c"]
        System.out.println(of); // ["a", "b", "c"]
        System.out.println(copy); // ["a", "b", "c"]

        copy.add("y"); // UnsupportedOperationException

        // EXAMPLES
        List<String> list = new ArrayList<>();
        list.add("SD"); // ["SD"]
        list.add(0, "NY"); // ["NY", "SD"]
        list.set(1, "FL"); // ["NY", "FL"]
        System.out.println(list.get(0)); // NY
        list.remove("NY"); // ["FL"]
        list.remove(0); // []
        list.set(0, "?"); // IndexOutOfBoundsException

        List<Integer> numbers = Arrays.asList(1, 2, 3);
        numbers.replaceAll(number -> number * 2);
        System.out.println(numbers); // [2,4,6]

        // Iterando lista
        for (int number : numbers) {
            System.out.println(number);
        }
        Iterator<Integer> iter = numbers.iterator();
        while (iter.hasNext()) {
            Integer num = iter.next();
            System.out.println(num);
        }

        ArrayList<String> in;
        List<String> result;
        var numA = new Integer[]{1, null, 3}; //1
        var list1 = List.of(numA); //2
        var list2 = Collections.unmodifiableList(list1); //3
        numA[1] = 2; //4
        System.out.println(list1+" "+list2);

    }

    public static <E extends CharSequence> List<? super E> doIt(List<E> nums){
        return nums;
    }
}
