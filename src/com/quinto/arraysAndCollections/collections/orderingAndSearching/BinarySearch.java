package com.quinto.arraysAndCollections.collections.orderingAndSearching;

// Rules
// La lista que se le pasa debe estar ordenada, sino el resultado es impredecible
// Devuelve el indice donde se encuentra el elemento

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BinarySearch {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(6, 9, 1, 8);
        Collections.sort(list);
        System.out.println(Collections.binarySearch(list, 6)); // 1
        System.out.println(Collections.binarySearch(list, 3)); // -3
        System.out.println("------------------------------------");

        var names = Arrays.asList("Fluffy", "Hoppy");
        Comparator<String> c = Comparator.reverseOrder();
        var index = Collections.binarySearch(names, "Hoppy", c);
        System.out.println(index); // El resultado es impredcible ya que la lista names no esta ordenada
    }
}
