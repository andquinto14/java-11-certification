package com.quinto.arraysAndCollections.collections.comparing;

// Has the method compareTo

// Rules
// 1. if x.equals(y) is true -> x.compareTo(y) is 0
// 2. if x.equals(y) is false -> x.compareTo(y) is not 0

import java.util.ArrayList;
import java.util.Collections;

class Duck implements Comparable<Duck> {
    private String name;

    public Duck(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Duck o) {
        return name.compareTo(o.name); // sorts ascending by name
    }

    public static void main(String[] args) {
        var ducks = new ArrayList<Duck>();
        ducks.add(new Duck("Quack"));
        ducks.add(new Duck("Puddles"));
        Collections.sort(ducks); // sort by name, the parameter should be a collection that implements Comparable
        System.out.println(ducks); // [Puddles, Quack]
    }
}

// compareTo returns
// 1. objects equivalents returns 0
// 2. current object is smaller than the argument returns a negative number
// 2. current object is higher than the argument returns a positive number
class Animal implements Comparable<Animal> {
    private int id;

    @Override
    public int compareTo(Animal o) {
        return id - o.id; // sorts ascending by id
    }

    public static void main(String[] args) {
        var a1 = new Animal();
        var a2 = new Animal();
        a1.id = 5;
        a2.id = 7;
        System.out.println(a1.compareTo(a2)); // -2
        System.out.println(a1.compareTo(a1)); // 0
        System.out.println(a2.compareTo(a1)); // 2
    }
}

// validating null
class MissingDuck implements Comparable<MissingDuck> {
    private String name;

    public MissingDuck(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(MissingDuck o) {
        if (o == null) {
            throw new IllegalArgumentException("Bad format");
        }
        if (this.name == null && o.name == null) {
            return 0;
        } else if (this.name == null) {
            return -1;
        } else if (o.name == null) {
            return 1;
        } else {
            return name.compareTo(o.name);
        }
    }
}

