package com.quinto.arraysAndCollections.collections.comparing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Duck2 implements Comparable<Duck2> {
    private String name;
    private int weight;

    public Duck2(String name, int weight) {
        this.name = name;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Duck2 o) {
        return name.compareTo(o.name); // sorts ascending by name
    }

    public static void main(String[] args) {
        Comparator<Duck2> byWeight = (o1, o2) -> o1.getWeight() - o2.getWeight();
        var ducks = new ArrayList<Duck2>();
        ducks.add(new Duck2("Lucas", 7));
        ducks.add(new Duck2("Donald", 10));
        Collections.sort(ducks); // sort by name
        System.out.println(ducks); // [Donald, Lucas]
        System.out.println("---------------------------------");
        Collections.sort(ducks, byWeight); // sort by weight
        System.out.println(ducks); // [Lucas, Donald]
    }
}

// Comparing bt multiple field
class Squirrel {
    private int weight;
    private String species;

    public Squirrel(int weight, String species) {
        this.weight = weight;
        this.species = species;
    }

    public int getWeight() {
        return weight;
    }

    public String getSpecies() {
        return species;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setSpecies(String species) {
        this.species = species;
    }
}

class MultipleFieldComparator implements Comparator<Squirrel> {
    @Override
    public int compare(Squirrel o1, Squirrel o2) {
        // Static methods
        /*Comparator<Squirrel> c = Comparator.comparing(Squirrel::getSpecies)
                .thenComparing(Squirrel::getWeight);
        var c1 = Comparator.comparing(Squirrel::getSpecies).reversed();*/

        int result = o1.getSpecies().compareTo(o2.getSpecies());
        if (result != 0) return result;
        return o1.getWeight() - o2.getWeight();
    }
}
