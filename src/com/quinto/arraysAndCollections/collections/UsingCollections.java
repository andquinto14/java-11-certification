package com.quinto.arraysAndCollections.collections;

// COLLECTIONS

// INTERFACES
// 1. List -> Ordered, admit duplicates
// 2. Set -> don´t allow duplicates
// 3. Queue -> order his elements in an specific order, FIFO, LIFO
// 4. Map -> don´t allow duplicates key, no hace parte de Collection

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class UsingCollections {
    public static void main(String[] args) {
        // List -> ArrayList, LinkedList
        // Queue -> LinkedList
        // Set -> HashSet, TreeSet
        // Map -> HasMap, TreeMap

        // COMMON METHODS
        // add
        System.out.println("------------------ADD-----------------------");
        Collection<String> list = new ArrayList<>();
        System.out.println(list.add("Sparrow")); // true
        System.out.println(list.add("Sparrow")); // true
        System.out.println("-----------------------------------------");
        Collection<String> list1 = new HashSet<>();
        System.out.println(list1.add("Sparrow")); // true
        System.out.println(list1.add("Sparrow")); // false

        // remove -> OJO: en List recibe el indice del elemento a remover, si no encuenta el indice arroja IndexOutOfBoundsException
        System.out.println("----------------REMOVE--------------------");
        Collection<String> list3 = new ArrayList<>();
        list3.add("hello");
        list3.add("hello");
        System.out.println(list3.remove("cardinal")); // false
        System.out.println(list3.remove("hello")); // true

        // Delete while iterate
        Collection<String> birds = new ArrayList<>();
        birds.add("hawk");
        birds.add("hawk");
        birds.add("hawk");
        for (String bird: birds){
            birds.remove(bird); // ConcurrentModificationException
        }

        // isEmpty(), size()
        System.out.println(birds.isEmpty()); // false
        System.out.println(birds.size()); // 3

        // clear()
        Collection<String> birds2 = new ArrayList<>();
        birds.add("hawk1");
        birds2.clear();
        System.out.println(birds2.isEmpty()); // true

        // contains()
        System.out.println(birds.contains("hawk")); // true
        System.out.println(birds.contains("hola")); // false

        // removeIf()
        Collection<String> birds3 = new ArrayList<>();
        birds.add("bird");
        birds.add("hello");
        birds3.removeIf(s -> s.startsWith("b"));
        System.out.println(birds3); // [hello]

        // forEach()
        Collection<String> cats = new ArrayList<>();
        cats.add("Molly");
        cats.add("Pepe");
        cats.forEach(System.out::println);

    }
}
