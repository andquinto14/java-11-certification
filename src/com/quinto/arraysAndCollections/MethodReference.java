package com.quinto.arraysAndCollections;

// FORMATS TO METHOD REFERENCES
// 1. Static methods
// 2. Instance methods on a particular object
// 3. Instance methods on a parameter
// 4. Constructors

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

@FunctionalInterface
interface LearnToSpeak {
    void speak(String sound);
}

class DuckHelper {
    public static void teacher(String name, LearnToSpeak trainer) {
        trainer.speak(name);
    }
}

class Duck {
    public static void makeSound(String sound) {
        LearnToSpeak learner = System.out::println;
        DuckHelper.teacher(sound, learner);
    }
}

// 1. Static methods
class StaticMethod{
    public static void main(String[] args) {
        Consumer<List<Integer>> methodRef = Collections::sort;
        Consumer<List<Integer>> lambda = x -> Collections.sort(x);
    }
}

// 2. Instance methods on a particular object
class ParticularInstanceMethod{
    public static void main(String[] args) {
        var str = "abc";
        Predicate<String> methodRef = str::startsWith;
        Predicate<String> lambda = s -> str.startsWith(s);
    }
}

// 3. Instance methods on a parameter
class EjcucionParameter{
    public static void main(String[] args) {
        Predicate<String> isEmpty = String::isEmpty;
        Predicate<String> lambda = s -> s.isEmpty();

        BiPredicate<String, String> stringStringBooleanBiFunction = String::startsWith;
        BiPredicate<String, String> lambdaString = (String s, String prefix) -> s.startsWith(prefix);
    }
}

// 4. Constructors
class Constructors {
    public static void main(String[] args) {
        Supplier<ArrayList> methodRef = ArrayList::new;
        Supplier<ArrayList> lambda = () -> new ArrayList<>();
        Function<Integer, ArrayList> methodRef1 = x -> new ArrayList(x);
    }
}