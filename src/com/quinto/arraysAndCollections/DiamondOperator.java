package com.quinto.arraysAndCollections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// SOME RULES
// 1. Only can be used on the right side of an assign operation

public class DiamondOperator {
    public static void main(String[] args) {
        // Before
        List<Integer> list = new ArrayList<Integer>();
        Map<String, Integer> map = new HashMap<String, Integer>();

        // After
        List<Integer> list2 = new ArrayList<>(); // Inferred type
        Map<String, Integer> map2 = new HashMap<>();

        // Bad uses
        /*List<> l = new ArrayList<Integer>();
        Map<> map3 = new HashMap<String, Integer>();
        void use(List<> l){

        }*/

        // Using var
        var lis = new ArrayList<>(); // create an array of object
        var lis2 = new ArrayList<Integer>(); // create an array of Integer
    }
}
