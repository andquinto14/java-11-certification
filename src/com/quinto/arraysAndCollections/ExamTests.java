package com.quinto.arraysAndCollections;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Function;

public class ExamTests {
    public static void main(String[] args) {
        List<String> vowels = new ArrayList<>();
        vowels.add("a");
        vowels.add("e");
        vowels.add("i");
        vowels.add("o");
        vowels.add("u");
        Function<List<String>, List<String>> f = list->list.subList(2, 4);
        f.apply(vowels);
        vowels.forEach(System.out::print);
    }
}

class SortTest {
    public static void main(String[] args) {
        Object[] sa = { 100, 100.0, "100" };
        Collections.sort(Arrays.asList(sa), null);
        System.out.println(sa[0]+" "+sa[1]+" "+sa[2] );
        int [] [] array = {{0}, {0, 1}, {0, 1, 2}, {0, 1, 2, 3}, {0, 1, 2, 3, 4}};
        var arr1 = array[4];
        //System.out.println (arr1[4][1]);
        System.out.println (array[4][1]);
    }

}

class Test{
    public static int[ ] getArray() {  return null;  }
    public static void main(String[] args){
        var index = 1;
        try{
            getArray()[index=2]++;
        }
        catch (Exception e){  }  //empty catch
        System.out.println("index = " + index);
    }
}

class TestClass {
    public static void main(String[] args)
    {
        // put declaration here
        Map m = new HashMap();

        m.put("1", new ArrayList());    //1
        m.put(1, new Object());    //2
        m.put(1.0, "Hello");     //3
        System.out.println(m);

        Deque<Integer> d = new ArrayDeque<>();
        d.add(1);
        d.add(2);
        d.addFirst(3);
        System.out.println(d.pollLast());
        System.out.println(d.pollLast());
        System.out.println(d.pollLast());

        Deque<Integer> d1 = new ArrayDeque<>();
        d1.add(1);
        d1.addFirst(2);
        d1.pop();
        d1.offerFirst(3);
        System.out.println(d1);

        List<String> list1 = List.of("A", "B");
        List<String> list2 = List.copyOf(list1);
        list1.add("C"); //1
        list2.add("D"); //2
        System.out.println(list1+" "+list2);
        int ia[][] = new int[][] { {1, 2}, null };
    }

    int i = 0 ;
    Object prevObject ;
    public void saveObject(List e ){
        prevObject = e ;
        i++ ;
    }
}

class Test2{
    public static void main(String[ ] args){
        var a = new int[]{ 1, 2, 3, 4 };
        int[] b = { 2, 3, 1, 0 };
        System.out.println( a [ (a = b)[3] ] );
    }
}

class ListTest{
    public static void main(String args[]){
        List s1 = new ArrayList( );
        s1.add("a");
        s1.add("b");
        s1.add(1, "c");
        List s2 = new ArrayList(  s1.subList(2, 2) );
        System.out.println(s1.subList(1, 1));
        s1.addAll(s2);
        System.out.println(s1);
    }
}

class TestClasss{
    public static void main(String args[ ] ){
        int i = 1;
        int[] iArr = {1};
        incr(i) ;
        incr(iArr) ;
        System.out.println( "i = " + i + "  iArr[0] = " + iArr [ 0 ] ) ;
    }
    public static void incr(int   n ) { n++ ; }
    public static void incr(int[ ] n ) { n [ 0 ]++ ; }
}

interface Birdie {
    void fly();
}

class Dino implements Birdie {
    public void fly(){ System.out.println("Dino flies"); }
    public void eat(){ System.out.println("Dino eats");}
}

class Bino extends Dino {
    public void fly(){ System.out.println("Bino flies"); }
    public void eat(){ System.out.println("Bino eats");}

}

class TestClassss {
    public static void main(String[] args)    {
        List<Birdie> m = new ArrayList<>();
        m.add(new Dino());
        m.add(new Bino());
        for(Birdie b : m) {
            b.fly();
            //b.eat();
        }
    }

    public void m1(List<? extends Number> list)
    {
        Number n = list.get(0);
    }
}

class eo{
    public static void main(String[] args) {
        HashSet<String> keys = new HashSet<>(List.of("a", "b", "c"));
        ArrayList<String> values = new ArrayList<>(Set.of("1", "2", "3"));

        Map<String, String> m = new HashMap<>();
        int i = 0;
        for(var key : keys){
            m.put(key, values.get(i++));
        }
        keys.clear();   //2
        values.clear();
        System.out.println(m.keySet().size()+" "+m.values().size());
    }
}

class FunWithArgs {
    public static void main(String[][] args) {
        System.out.println(args[0][1]);
    }
    public static void main(String[] args) {
        var fwa = new FunWithArgs();
        String[][] newargs = {args};
        fwa.main(newargs);
    }
}

class MyStringComparator implements Comparator
{
    public int compare(Object o1, Object o2)
    {
        int s1 = ((String) o1).length();
        int s2 = ((String) o2).length();
        return s1 - s2;
    }

    public static void main(String[] args) {
        var nameList = new ArrayList<String>();
        nameList.add("Ally");
        nameList.add("Billy");
        nameList.add("Cally");
        nameList.add("Billy");
        nameList.add("Ally");

        var nameSet1 = new HashSet<String>();
        for(var name:nameList) nameSet1.add(name);
        var nameSet2 = new HashSet<String>(nameList);
        System.out.println(nameList.size()+" "+nameSet1.size()+" "+nameSet2.size());

        Float f = 2.0f;
        Byte b = 2;
        Character c = 2;

        Map hm = new ConcurrentHashMap();
        hm.put(null, "asdf");  //1
        hm.put("aaa", null);  //2

        hm = new HashMap();
        hm.put(null, "asdf");  //3
        hm.put("aaa", null);  //4

        List list = new ArrayList();
        list.add(null); //5
        list.add(null); //6

        list = new CopyOnWriteArrayList();
        list.add(null); //7

        int [] [] array = {{0}, {0, 1}, {0, 1, 2}, {0, 1, 2, 3}, {0, 1, 2, 3, 4}};
        var arr1 = array[4];
        //System.out.println (arr1[4][1]);
        System.out.println (array[4][1]);

    }
}

class MyGenericClass<T>{
    public <T> String transform(T t){
        return t.toString()+"-"+t.hashCode();
    }

    public static void main(String[] args) {
        MyGenericClass gc = new MyGenericClass();
        System.out.println(gc.transform(1)); //1
        System.out.println(gc.transform("hello")); //2
        MyGenericClass<String> gcStr = new MyGenericClass<String>();
        System.out.println(gcStr.transform(1.1)); //3

    }
}

class ArrayTest{
    public static void main(String[] args){
        var ia = new int[][]{ {1, 2}, null };
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                System.out.println(ia[i][j]);
    }
}

class TestClassv {
    public static void main(String[] args) throws Exception {
        List  al = new ArrayList(); //1
        al.add(111); //2
        System.out.println(al.get(al.size()));  //3
    }
}

class Testcv{
    public static int[ ] getArray() {  return null;  }
    public static void main(String[] args){
        var index = 1;
        try{
            getArray()[index=2]++;
        }
        catch (Exception e){  }  //empty catch
        System.out.println("index = " + index);
    }
}

// List, Set, SortedSet, OrderedSet, OrderedList, NavigableSet

class TestClassb
{
    static String[] sa = { "a", "aa", "aaa", "aaaa" };
    static
    {
        Arrays.sort(sa);
    }
    public static void main(String[] args)
    {
        String search = "";
        if(args.length != 0) search = args[0];
        System.out.println(Arrays.binarySearch(sa, search));

        List<String> s1 = new ArrayList<String>();
        String s = null;
        s1.add("a");
        s1.add(s); //1
        s1.add("b");
        s1.remove(s); //2
        System.out.println(s1); //3

        ArrayList<CharSequence> in = new ArrayList<>();
        List result;
        result = doIt(in);

        Deque<Integer> d = new ArrayDeque<>();
        d.add(1);
        d.addFirst(2);
        d.pop();
        d.offerFirst(3);
        System.out.println(d);

        int [] a = new int[0];
        a[0] = 2;
    }
    public static <E extends CharSequence> List<? super E> doIt(List<E> nums){
        return nums;
    }

}

class Learner {
    public static void main(String[] args) {
        var dataArr = new String[4];
        dataArr[1] = "Bill";
        dataArr[2] = "Steve";
        dataArr[3] = "Larry";
        try{
            for(String data : dataArr){
                System.out.print(data+" ");
            }
            Deque<Integer> d = new ArrayDeque<>();
            d.push(1);
            d.push(2);
            d.push(3);
            System.out.println(d.pollFirst());
            System.out.println(d.poll());
            System.out.println(d.pollLast());

            int[][] twoD = { { 1, 2, 3} , { 4, 5, 6, 7}, { 8, 9, 10 } };
            System.out.print(twoD[1].length);
            System.out.print(twoD[2].getClass().isArray());
            System.out.print(twoD[1][2]);
            System.out.print(twoD[0] == twoD.clone()[0]);
        }catch(Exception e){
            System.out.println(e.getClass());
        }
    }
}

class Testss{
    int[] ia = new int[1];
    Object oA[]  = new Object[1];
    boolean bool;
    public static void main(String args[]){
        var test = new Testss();
        System.out.println(test.ia[0] + "  " + test.oA[0]+"  "+test.bool);
    }

    public void m1(List<? extends Number> list)
    {
        Number n = list.get(0);
    }
}