package com.quinto.arraysAndCollections.generics;

// NAMING CONVENTIONS
// E -> for an element
// K -> for a map key
// V -> for a map value
// N -> for a number
// T -> for a generic data type
// S,U,V, and so forth for multiple generic types


// Using 1 parameter
class Crate<T> {
    private T contents;

    public T emptyCrate() {
        return contents;
    }

    void packCrate(T contents) {
        this.contents = contents;
    }
}

class Elephant {

}

class Robot {

}

class Tests {
    public static void main(String[] args) {
        Elephant elephant = new Elephant();
        Crate<Elephant> crateForElephant = new Crate<>();
        crateForElephant.packCrate(elephant);
        Elephant inHome = crateForElephant.emptyCrate();

        Robot robot = new Robot();
        Crate<Robot> crateForRobot = new Crate<>();
        crateForRobot.packCrate(robot);
        Robot atDestination = crateForRobot.emptyCrate();
    }
}

// Using multiple parameters
class SizeLimitedCrate<T, U> {
    private T contents;
    private U sizeLimit;

    public SizeLimitedCrate(T contents, U sizeLimit) {
        this.contents = contents;
        this.sizeLimit = sizeLimit;
    }
}

class Test2 {
    Elephant elephant = new Elephant();
    Integer numPounds = 15_000;
    SizeLimitedCrate<Elephant, Integer> c1 = new SizeLimitedCrate<>(elephant, numPounds);
}