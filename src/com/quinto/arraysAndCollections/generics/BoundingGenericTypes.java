package com.quinto.arraysAndCollections.generics;

//

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BoundingGenericTypes {
    public static void main(String[] args) {
        // Unbounded wildcard -> ?
        List<?> a1 = new ArrayList<String>();

        // Wildcard with an upper bound -> ? extends type
        List<? extends Exception> a2 = new ArrayList<RuntimeException>();

        // Wildcard with a lower bound -> ? super type
        List<? super Exception> a3 = new ArrayList<Object>();
    }
}

// 1. Unbounded wildcard
class Example1 {
    public static void printList(List<Object> list) {
        for (Object x : list) {
            System.out.println(x);
        }
    }

    public static void printListWildcard(List<?> list) {
        for (Object x : list) {
            System.out.println(x);
        }
    }

    public static void main(String[] args) {
        // Before wildcards
        List<String> keywords = new ArrayList<>();
        keywords.add("java");
        //printList(keywords); NO COMPILE, method printList expect a list of Object
        List<Integer> number = new ArrayList<>();
        number.add(new Integer(3));
        //List<Object> objects = number; NO COMPILE

        // With wildcards
        List<String> keywords2 = new ArrayList<>();
        keywords2.add("java");
        printListWildcard(keywords);

        // Example
        List<?> x1 = new ArrayList<>(); // Type List
        var x2 = new ArrayList<>(); // Type ArrayList
    }
}

// 2. Upper-Bounded wildcard
class Example2 {
    public static void main(String[] args) {
        // Before
        // ArrayList<Number> list = new ArrayList<Integer>(); NO COMPILE

        // With wildcards
        List<? extends Number> list = new ArrayList<Integer>();
    }

    public static long total(List<? extends Number> list) {
        long count = 0;
        for (Number number : list) {
            count += number.longValue();
        }
        return count;
    }

}

class ImmutableList {
    // La lista se convierte en inmutable
    static class Sparrow extends Bird {

    }

    static class Bird {
    }

    public static void main(String[] args) {
        List<? extends Bird> birds = new ArrayList<Bird>();
        // birds.add(new Sparrow()); NO COMPILE
        // birds.add(new Bird()); NO COMPILE
    }
}

// 2. Lower-Bounded wildcard
class Example3 {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        strings.add("tweet");

        List<Object> objects = new ArrayList<>(strings);
        addSound(strings);
        addSound(objects);

        List<? super IOException> exceptions = new ArrayList<Exception>();
        //exceptions.add(new Exception()); // NO COMPILE
        exceptions.add(new IOException());
        exceptions.add(new FileNotFoundException());
    }

    private static void addSound(List<? super String> list) {
        System.out.println(list);
    }
}