package com.quinto.arraysAndCollections.generics;

// NAMING CONVENTIONS
// E -> for an element
// K -> for a map key
// V -> for a map value
// N -> for a number
// T -> for a generic data type
// S,U,V, and so forth for multiple generic types

class Handler {

    // Antes del tipo de retorno hay que poner el tipo generico
    // El tipo de generico declarado en la clase es independiente del que se declare en el metodo

    public static <T> void prepare(T t){
        System.out.println("Preparing: " + t);
    }

    public static <T> Crate<T> ship(T t){
        System.out.println("Shipping: " + t);
        return new Crate<>();
    }
}

class More{
    public static <T> void sink(T t){}
    public static <T> T identity(T t){
        return t;
    }
    /*public static T noGood(T t){ // should be <T>
        return t;
    }*/
}