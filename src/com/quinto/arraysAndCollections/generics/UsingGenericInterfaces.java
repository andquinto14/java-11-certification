package com.quinto.arraysAndCollections.generics;

// NAMING CONVENTIONS
// E -> for an element
// K -> for a map key
// V -> for a map value
// N -> for a number
// T -> for a generic data type
// S,U,V, and so forth for multiple generic types


// Using 1 parameter
interface Shippable<T>{
    void ship(T t);
}

// 1. Specifying type of object in the class that implements the generic interface
class ShippableRobotCrate implements Shippable<Robot> {
    public void ship(Robot t){

    }
}

// 2. Creating a generic class that implements the generic interface
class ShippableAbstractCrate<U> implements Shippable<U> {
    public void ship(U t){

    }
}

// 3. Without generics
class ShippableCrate implements Shippable { // Raw type
    public void ship(Object t){

    }
}

