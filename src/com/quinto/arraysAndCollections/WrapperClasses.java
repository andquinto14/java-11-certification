package com.quinto.arraysAndCollections;

import java.util.ArrayList;
import java.util.List;

public class WrapperClasses {
    public static void main(String[] args) {
        Integer pounds = 120; // Autoboxing
        Character letter = "robot".charAt(0); // Autoboxing
        char r = letter; // Unboxing

        var heights = new ArrayList<Integer>();
        heights.add(null);
        int h = heights.get(0); // NullPointerException, no se puede hacer ese unboxing

        List<Integer> numbers = new ArrayList<>();
        numbers.add(1); // Autoboxing
        numbers.add(Integer.valueOf(3));
        numbers.add(Integer.valueOf(5));
        numbers.remove(1); // Borra el del indice 1, queda [1, 5]
        numbers.remove(Integer.valueOf(5)); // Borra el que tenga el valor 5, queda [1]
        System.out.println(numbers);
    }
}
